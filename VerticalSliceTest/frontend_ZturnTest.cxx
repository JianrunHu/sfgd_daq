/********************************************************************\

  Name:         frontend.c
  Created by:   Stefan Ritt

  Contents:     Experiment specific readout code (user part) of
                Midas frontend. This example simulates a "trigger
                event" and a "periodic event" which are filled with
                random data.
 
                The trigger event is filled with two banks (ADC0 and TDC0),
                both with values with a gaussian distribution between
                0 and 4096. About 100 event are produced per second.
 
                The periodic event contains one bank (PRDC) with four
                sine-wave values with a period of one minute. The
                periodic event is produced once per second and can
                be viewed in the history system.

\********************************************************************/

#undef NDEBUG // midas required assert() to be always enabled

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h> // assert()

#include "midas.h"
#include "experim.h"

#include "mfe.h"

// fake data generation
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <time.h>
#include <errno.h>

//#include <iostream>
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;
using std::chrono::nanoseconds;
using std::chrono::duration_cast;

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
const char *frontend_name = "Sample Frontend";
/* The frontend file name, don't change it */
const char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;
//BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 3000;

/* maximum event size produced by this frontend */
INT max_event_size = 1024 * 1024; // 1 MB

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024; // 5 MB

/* buffer size to hold events */
INT event_buffer_size = 10 * 1024 * 1024; // 10 MB, must be > 2 * max_event_size

/*-- Function declarations -----------------------------------------*/

INT frontend_init(void);
INT frontend_exit(void);
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop(void);

INT read_trigger_event(char *pevent, INT off);
INT read_periodic_event(char *pevent, INT off);

INT poll_event(INT source, INT count, BOOL test);
INT interrupt_configure(INT cmd, INT source, POINTER_T adr);

// fake data generation
const int AXI_REG_BASE = 0x43c00000; // pointer to AXI register, defined in Verilog
const int REG_SIZE = 0x20; // 32 registers (16 each: RW and RO)
const int RESET_OFFSET = 1; // reset firmware
const int DATA_START_OFFSET = 2; // start fake data generation
const int DATA_READ_OFFSET = 3; // read next data in buffer (set after reading current data)
const int BUFFER_FULL_OFFSET = 17; // data buffer in firmware full
const int DATA_AVAILABLE_OFFSET = 18; // data availble flag (to be polled by DAQ)
const int DATA_OFFSET = 19; // fake data (32-bits integer incremented by 1)
/// map memory address
int mem_fd = 0; // /dev/mem memory file descriptor
int *reg_ptr; // pointer to AXI register

//histograming
const int Nrepli=1;
const int NHist = 2000;
const int NBins=100;
int EBins[NBins+1];
int **HistoBins; // from 0 to 32000
int triggerLED=0;
int **HistoTimes; // from 0 to NBins
int timeSave;
int mode=2;

/*-- Equipment list ------------------------------------------------*/
int generate_fake_data(bool fQuiet);
int HistBinning(int inputNo, int fMode);

BOOL equipment_common_overwrite = TRUE;

EQUIPMENT equipment[] = {

   {"Trigger",               /* equipment name */
      {1, 0,                 /* event ID, trigger mask */
         "SYSTEM",           /* event buffer */
         EQ_POLLED,          /* equipment type */
         0,                  /* event source */
         "MIDAS",            /* format */
         TRUE,               /* enabled */
         RO_RUNNING,        /* read only when running */
         100,                /* poll for 100ms */
         0,                  /* stop run after this event limit */
         0,                  /* number of sub events */
         0,                  /* don't log history */
         "", "", "", "", "", 0, 0},
      read_trigger_event,    /* readout routine */
   },

   {"Periodic",              /* equipment name */
      {2, 0,                 /* event ID, trigger mask */
         "SYSTEM",           /* event buffer */
         EQ_PERIODIC,        /* equipment type */
         0,                  /* event source */
         "MIDAS",            /* format */
         TRUE,               /* enabled */
         RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
         RO_ODB,             /* and update ODB */
         1000,               /* read every 1 sec */
         0,                  /* stop run after this event limit */
         0,                  /* number of sub events */
         10,                 /* log history every ten seconds*/
       "", "", "", "", "", 0, 0},
      read_periodic_event,   /* readout routine */
   },

   {""}
};

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
   /* put any hardware initialization here */
   // open /dev/mem
   mem_fd = open("/dev/mem", O_RDWR);
   printf ("Opening /dev/mem. mem_fd: 0x%x\n", mem_fd);
   if (mem_fd == -1) {
   	printf ("Error opening /dev/mem. mem_fd: 0x%x\n", mem_fd);
   	return 0;
   }
   // memory map AXI register from /dev/mem to user space
   reg_ptr = (int*)mmap(NULL, REG_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, AXI_REG_BASE);
   if (*reg_ptr == -1) {
   	printf ("Error during mmap. reg_ptr: 0x%x\n", *reg_ptr);
   	return 0;	
   }
   
   // initialing the binning method
   for(int binid=0;binid<NBins+1;binid++){
       EBins[binid]=binid;
   }

   /* print message and return FE_ERR_HW if frontend should not be started */
   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
   // unmap memory
   munmap(reg_ptr, REG_SIZE);

   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
   /* put here clear scalers etc. */
   // initialing the histogram
   HistoBins=new int*[NHist]; // from 0 to NBins
   for(int histid=0;histid<NHist;histid++){
	HistoBins[histid]=new int[NBins];
   for(int binid=0;binid<NBins;binid++){
   	HistoBins[histid][binid]=0;
   }
   }
   HistoTimes=new int*[NHist]; // from 0 to NBins
   for(int histid=0;histid<NHist;histid++){
	HistoTimes[histid]=new int[NBins];
   for(int binid=0;binid<NBins;binid++){
   	HistoTimes[histid][binid]=0;
   }
   }

   return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
   return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;
   DWORD flag;
   
   for (i = 0; i < count; i++) {
      /* poll hardware and set flag to TRUE if new event is available */
      flag = TRUE;
      if (flag)
         if (!test){

         //int result=generate_fake_data(false);
         int result=rand()%NBins;
         int binSet=HistBinning(result, mode);
         int histSet=(result+1234)%NHist;
         //printf("Histograming: binSet is %d \r\n", binSet);
         if(histSet<0 || histSet>=NHist) return 0;
         if(binSet<0 || binSet>=NBins) return 0;
         HistoBins[histSet][binSet]++;
         triggerLED++;
         //printf("Histograming: triggerLED is %d : binSet %d \r\n", triggerLED, binSet);

   	   if(triggerLED>1E4){
   	      triggerLED=0;
              return TRUE;
   	   }
         }
   }

   return 0;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{

   //auto t1=Clock::now();
   DWORD start_time=ss_millitime();
   UINT32 *pdata;

   /* init bank structure */
   bk_init32(pevent);

   /* create structured ADC0 bank */
   bk_create(pevent, "ADC0", TID_UINT32, (void **)&pdata);

   /* following code to "simulates" some ADC data */
   for (int histid = 0; histid < NHist; histid++){
   for (int i = 0; i < NBins; i++){
      //*pdata++ = rand()%1024 + rand()%1024 + rand()%1024 + rand()%1024;
      *pdata++=HistoBins[histid][i];
   }
   }

   bk_close(pevent, pdata);

// replicate
   if(Nrepli>1){
   for(int repli=1;repli<Nrepli;repli++){
   /* create structured ADC0 bank */
   //char bufferName[5];
   char *bufferName=new char[5];
   sprintf(bufferName, "ADC%d", repli);
   bk_create(pevent, bufferName, TID_UINT32, (void **)&pdata);

   /* following code to "simulates" some ADC data */
   for (int histid = 0; histid < NHist; histid++){
   for (int i = 0; i < NBins; i++){
      //*pdata++ = rand()%1024 + rand()%1024 + rand()%1024 + rand()%1024;
      *pdata++=HistoBins[histid][i];
   }
   }

   bk_close(pevent, pdata);
   delete bufferName;
   }
   }
// end of the replication

   //triggerLED=0;
   DWORD end_time=ss_millitime();
   //auto t2=Clock::now();
   //nanoseconds ns = duration_cast<nanoseconds>(t2-t1);

   printf("Histograming has finished for one round: %d \r\n", end_time-start_time);
   FILE* fp;
   fp=fopen("/home/ocb/online/tempfile0.log","a");
   fprintf(fp, "% d\n", end_time-start_time);
   fclose(fp);

   // fake data generation
   /*
   int *histarray=new int[NHist];
   for(int binid=0;binid<NHist;binid++){
	   histarray[binid]=rand()%NBins;
   }
   */
   int **randomarray=new int*[NHist];
   for(int histid=0;histid<NHist;histid++){
           randomarray[histid]=new int[NBins];
   for(int binid=0;binid<NBins;binid++){
	   randomarray[histid][binid]=rand()%NBins;
	   //randomarray[histid][binid]=rand();
   }
   }
   DWORD start_time2=ss_millitime();
   for(int repli=0;repli<Nrepli;repli++){
   for(int histid=0;histid<NHist;histid++){
   for(int ii=0;ii<NBins;ii++){
   //int binSet=HistBinning(histarray[ii], mode);
   int binSet=HistBinning(randomarray[histid][ii], mode);
   //printf("Histograming: binSet is %d \r\n", binSet);
   if(binSet<0 || binSet>=NBins) continue;
   HistoTimes[histid][binSet]++;
   }
   }
   }
   DWORD end_time2=ss_millitime();
   timeSave=end_time2-start_time2;
   //delete histarray;
   delete randomarray;

   FILE* fp2;
   fp2=fopen("/home/ocb/online/tempfile1.log","a");
   fprintf(fp2, "% d\n", timeSave);
   fclose(fp2);

   ///* create variable length TDC bank */
   bk_create(pevent, "TDC0", TID_UINT32, (void **)&pdata);

   ///* following code to "simulates" some TDC data */
   *pdata++ = end_time-start_time;
   *pdata++ = timeSave;
   //for (int i = 0; i < 4; i++)
   //   *pdata++ = rand()%1024 + rand()%1024 + rand()%1024 + rand()%1024;

   bk_close(pevent, pdata);

   /* limit event rate to 1000 Hz. In a real experiment remove this line */
   ss_sleep(1);

   return bk_size(pevent);
}

/*-- Periodic event ------------------------------------------------*/

INT read_periodic_event(char *pevent, INT off)
{
   UINT32 *pdata;

   /* init bank structure */
   bk_init32(pevent);

   /* create SCLR bank */
   bk_create(pevent, "PRDC", TID_UINT32, (void **)&pdata);

   /* following code "simulates" some values in sine wave form */
   for (int i = 0; i < 16; i++){
      //*pdata++ = 100*sin(M_PI*time(NULL)/60+i/2.0)+100;
      //int result=generate_fake_data(false);
      int result=rand()/32000;
      if(i==0) printf("Fake data in pdata: %d \r\n", result);
      *pdata++=result;
   }

   bk_close(pevent, pdata);

   return bk_size(pevent);
}


// fake data generation
int generate_fake_data(bool fQuiet){
	int result=0;

	bool data_available = *(reg_ptr+DATA_AVAILABLE_OFFSET);

	// poll data available flag
	while (!data_available) {
		data_available = *(reg_ptr+DATA_AVAILABLE_OFFSET);
		if(!fQuiet) printf("Polling for data available flag ... \r\n");
		usleep(1000);
	}

	//if(!fQuiet) printf("Data available! \r\n");

	// get fake data
	if (data_available) {
		// print fake data
		//printf("Fake data: %d \r\n", *(reg_ptr+DATA_OFFSET));
		result=*(reg_ptr+DATA_OFFSET);

	}

	//printf("Data read test completed! \r\n");

	return result;
}

int HistBinning(int inputNo, int fMode){
	int result=-1;
	switch(fMode){
	   case 0:
	   {
  	      result=inputNo%NBins;
	      break;
	   }
	   case 1:
	   {
   	      for(int binid=0;binid<NBins+1;binid++){
   	          if(inputNo>=EBins[binid] && inputNo<EBins[binid+1]){
			  result=binid;
			  break;
		  }
   	      }
	      break;
	   }
	   case 2:
	   {
	      int binStart=0, binEnd=NBins, binMiddle=int(NBins/2);
	      while(binStart+1==binEnd){
   	          if(inputNo>=EBins[binStart] && inputNo<EBins[binMiddle]){
			  binEnd=binMiddle;
		  }
   	          else if(inputNo>=EBins[binMiddle] && inputNo<EBins[binEnd]){
			  binStart=binMiddle;
		  }
		  binMiddle=binStart+int((binEnd-binStart)/2);
	      }
	      result=binStart;
	      break;
	   }
	   default: 
	      printf ("Please check your fMode: %d\n", fMode);
	}
	return result;
}
