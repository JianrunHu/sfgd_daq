/********************************************************************\

  Name:         frontend.c
  Created by:   Stefan Ritt

  Contents:     Experiment specific readout code (user part) of
                Midas frontend. This example simulates a "trigger
                event" and a "periodic event" which are filled with
                random data.
 
                The trigger event is filled with two banks (ADC0 and TDC0),
                both with values with a gaussian distribution between
                0 and 4096. About 100 event are produced per second.
 
                The periodic event contains one bank (PRDC) with four
                sine-wave values with a period of one minute. The
                periodic event is produced once per second and can
                be viewed in the history system.

\********************************************************************/

#undef NDEBUG // midas required assert() to be always enabled

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h> // assert()

#include "midas.h"
#include "experim.h"

#include "mfe.h"

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sys/ioctl.h>
#include <time.h>
extern "C" {
#include "utility.h"
};

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
const char *frontend_name = "Sample Frontend";
/* The frontend file name, don't change it */
const char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 3000;

/* maximum event size produced by this frontend */
INT max_event_size = 1024 * 1024; // 1 MB

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024; // 5 MB

/* buffer size to hold events */
INT event_buffer_size = 10 * 1024 * 1024; // 10 MB, must be > 2 * max_event_size

/*-- Function declarations -----------------------------------------*/

INT frontend_init(void);
INT frontend_exit(void);
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop(void);

INT read_trigger_event(char *pevent, INT off);
INT read_periodic_event(char *pevent, INT off);

INT poll_event(INT source, INT count, BOOL test);
INT interrupt_configure(INT cmd, INT source, POINTER_T adr);

/*-- Equipment list ------------------------------------------------*/

BOOL equipment_common_overwrite = TRUE;

EQUIPMENT equipment[] = {

   {"Trigger",               /* equipment name */
      {1, 0,                 /* event ID, trigger mask */
         "SYSTEM",           /* event buffer */
         EQ_POLLED,          /* equipment type */
         0,                  /* event source */
         "MIDAS",            /* format */
         TRUE,               /* enabled */
         RO_RUNNING,        /* read only when running */
         100,                /* poll for 100ms */
         0,                  /* stop run after this event limit */
         0,                  /* number of sub events */
         0,                  /* don't log history */
         "", "", "", "", "", 0, 0},
      read_trigger_event,    /* readout routine */
   },

   {"Periodic",              /* equipment name */
      {2, 0,                 /* event ID, trigger mask */
         "SYSTEM",           /* event buffer */
         EQ_PERIODIC,        /* equipment type */
         0,                  /* event source */
         "MIDAS",            /* format */
         TRUE,               /* enabled */
         RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */
         RO_ODB,             /* and update ODB */
         1000,               /* read every sec */
         0,                  /* stop run after this event limit */
         0,                  /* number of sub events */
         10,                 /* log history every ten seconds*/
       "", "", "", "", "", 0, 0},
      read_periodic_event,   /* readout routine */
   },

   {""}
};

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
   /* put any hardware initialization here */
// data readout slow control arguments
const unsigned int DATA_READOUT_START = 0x0;
const unsigned int DATA_READOUT_STOP = 0x1;
// number of FEBs per crate
const int n_feb = 14;
	float filesize=0.001;
	//int slot_id[14];
  int slot_id[14]={-1, -1, -1, -1,-1,-1,-1, -1, -1, -1, -1, -1, -1, 13};

	// encode crate ID and slot ID into board ID
  unsigned int board_id[n_feb];
	int n_board = 0;
  for (int idx=0; idx<n_feb; ++idx) {
    if (slot_id[idx] == -1) board_id[idx] = slot_id[idx];
    else {
			board_id[idx] = (CRATE_ID<<4) | slot_id[idx];
			++n_board;
		}
		printf("board_id %d: %d ", idx, board_id[idx]);
  }

	// map memory address
	int *reg_ptr;
	int mem_fd = 0; // /dev/mem memory file descriptor

	// open /dev/mem
	mem_fd = open("/dev/mem", O_RDWR);
	if (mem_fd == -1) {
		printf("Error opening /dev/mem. mem_fd: 0x%x\n", mem_fd);
		return -1;
	}

	// memory map AXI register from /dev/mem to user space
	reg_ptr = (int*)mmap(NULL, REG_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, REG_BASE);
	if (*reg_ptr == -1) {
		printf("Error during mmap. reg_ptr: 0x%x\n", *reg_ptr);
		return -1;	
	}
	printf(" reg_ptr: 0x%x\n", *reg_ptr);

	// open file
  FILE *fp;
  int buflen = 20;
  char currline[buflen];  
	char *filename = "test.bin";
	fp = fopen(filename, "wb");		
  if (fp == NULL) {
  	printf("Error opening file %s\n", filename);
		return -1;	
  }

	// reset OCB firmware
  send_reset(reg_ptr); 

	// encode slot enable
	int slot_enable = 0;
	printf("Enable slot: ");
	for (int idx=0; idx<n_feb; ++idx) {
		if (slot_id[(n_feb-1)-idx] == -1) slot_enable = (slot_enable<<1) | 0;
		else {
			printf("%d ", (n_feb-1)-idx);
			slot_enable = (slot_enable<<1) | 1;
		}
		printf("slot_enable: %d ", slot_enable);
	}
	printf("\n");

	// set enable start
	printf("Set enable start\n");
	start_data_readout(reg_ptr, slot_enable);

	const int head = 0x8;
	const int cmd = 0x0;
	unsigned int word;
	int status;

	// send data readout start slow control request for given FEB board IDs
	printf("Start data readout from FEB ");
	for (int idx=0; idx<n_feb; ++idx) {
		if (board_id[idx] == -1) continue;
		printf("%d ", board_id[idx]);

		// get only the 7-LSB of the board ID
		int board = get_bits(255, 0, 7);

		word = (head<<28) | (board<<21) | (cmd<<16) | DATA_READOUT_START;
	  printf("word is set here: %d, %d, %d, %d, %d\n", head, board, cmd, DATA_READOUT_START, word);
		write_slow_control(reg_ptr, word);
	}
	printf("\n");

	// read all answers from request writes
	int n = 0;
   printf("reg_ptr: 0x%x\n", *reg_ptr);
   printf(" Slow control!!!:  %d, %d  \n", SLOW_CONTROL_ANSWER_EMPTY_OFFSET, !fifo_empty_timeout(reg_ptr, SLOW_CONTROL_ANSWER_EMPTY_OFFSET) );
    while (!fifo_empty_timeout(reg_ptr, SLOW_CONTROL_ANSWER_EMPTY_OFFSET)) {
		status = read_slow_control(reg_ptr);
		++n;
	}
	if (n != n_board) printf("DATA_READOUT_START: %d slow control answers received (%d expected)\n", n, n_board);

	int special_word_empty = read_reg(reg_ptr, SPECIAL_WORD_EMPTY_OFFSET);

	// write data to output binary file
	filesize *= 1e6;
	float currsize = 0;
	while ((currsize<filesize) && !data_av_timeout(reg_ptr)) {
  	while (!fifo_empty_timeout(reg_ptr, DATA_EMPTY_OFFSET)) {
			printf("\rProgress: %.2f KB of %.2f MB written ...", currsize/1e3, filesize/1e6);
			fflush(stdout);
		  //cm_yield(1); //contact the midas for 1ms

			uint32_t data = read_data(reg_ptr);
			fwrite(&data, sizeof(data), 1, fp);
			currsize += 4;

			// read special word if any
			special_word_empty = read_reg(reg_ptr, SPECIAL_WORD_EMPTY_OFFSET);
			if (!special_word_empty) {
				int special_word = read_special_word(reg_ptr);
				int board = get_bits(special_word, 20, 8);
				int id = get_bits(special_word, 0, 20);
				if (id == 0x10000) printf("FEB %d: Readout end\n", board);
				else if (id == 0x20000) printf("FEB %d: GTS tag reset\n", board);
				else if (id == 0x40000) printf("FEB %d: Gate number reset\n", board);
				else if (id == 0x60000) printf("FEB %d: GTS tag + gate number reset\n", board);
				else if (id == 0x0FFFF) {
					if (get_bits(special_word, 5, 1) == 1) printf("FEB %d: L0 FIFO full\n", board);
					if (get_bits(special_word, 4, 1) == 1) printf("FEB %d: L1 FIFO full\n", board);
					if (get_bits(special_word, 3, 1) == 1) printf("FEB %d: L2 FIFO full\n", board);
					if (get_bits(special_word, 2, 1) == 1) printf("FEB %d: GTS lost\n", board);
					if (get_bits(special_word, 1, 1) == 1) printf("FEB %d: SYNC decoder error\n", board);
					if (get_bits(special_word, 0, 1) == 1) printf("FEB %d: Ext clk error\n", board);
				}
			}
		}
	}
	printf("\n");
	if (currsize < filesize) printf("Timeout for data available flag!\n");
	else printf("File size limit reached!\n");

	// send data readout stop slow control request for given FEB board IDs
	printf("Stop data readout from FEB ");
	for (int idx=0; idx<n_feb; ++idx) {
		if (board_id[idx] == -1) continue;
		printf("%d ", board_id[idx]);

		// get only the 7-LSB of the board ID
		int board = get_bits(255, 0, 7);

		word = (head<<28) | (board<<21) | (cmd<<16) | DATA_READOUT_STOP;
		write_slow_control(reg_ptr, word);
	}
	printf("\n");

	// read all answers from request writes
	n = 0;
  while (!fifo_empty_timeout(reg_ptr, SLOW_CONTROL_ANSWER_EMPTY_OFFSET)) {
		status = read_slow_control(reg_ptr);
		++n;
	}
	if (n != n_board) printf("DATA_READOUT_STOP: %d slow control answers received (%d expected)\n", n, n_board);

	// set enable stop
	printf("Set enable stop\n");
	stop_data_readout(reg_ptr);

	// write remaining data in FIFO (if any) to output binary file
	printf("Write remaining data (if any) to output file ...\n");
	while (!data_av_timeout(reg_ptr)) {
  	while (!fifo_empty_timeout(reg_ptr, DATA_EMPTY_OFFSET)) {
			int data = read_data(reg_ptr);
			fwrite(&data, sizeof(data), 1, fp);
		}
	}
	printf("Timeout for data available flag!\n");

	// read special word if any
	printf("Read remaining special word (if any) ...\n");
  while (!fifo_empty_timeout(reg_ptr, SPECIAL_WORD_EMPTY_OFFSET)) {
		int special_word = read_special_word(reg_ptr);
		int board = get_bits(special_word, 20, 8);
		int id = get_bits(special_word, 0, 20);
		if (id == 0x10000) printf("FEB %d: Readout end\n", board);
		else if (id == 0x20000) printf("FEB %d: GTS tag reset\n", board);
		else if (id == 0x40000) printf("FEB %d: Gate number reset\n", board);
		else if (id == 0x60000) printf("FEB %d: GTS tag + gate number reset\n", board);
		else if (id == 0x0FFFF) {
			if (get_bits(special_word, 5, 1) == 1) printf("FEB %d: L0 FIFO full\n", board);
			if (get_bits(special_word, 4, 1) == 1) printf("FEB %d: L1 FIFO full\n", board);
			if (get_bits(special_word, 3, 1) == 1) printf("FEB %d: L2 FIFO full\n", board);
			if (get_bits(special_word, 2, 1) == 1) printf("FEB %d: GTS lost\n", board);
			if (get_bits(special_word, 1, 1) == 1) printf("FEB %d: SYNC decoder error\n", board);
			if (get_bits(special_word, 0, 1) == 1) printf("FEB %d: Ext clk error\n", board);
		}
	}
	printf("Timeout for special word FIFO empty!\n");

	// unmap memory
	munmap(reg_ptr, REG_SIZE);

	// close file
  fclose(fp);

   /* print message and return FE_ERR_HW if frontend should not be started */
   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
   /* put here clear scalers etc. */

   return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
   return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;
   DWORD flag;

   for (i = 0; i < count; i++) {
      /* poll hardware and set flag to TRUE if new event is available */
      flag = TRUE;

      if (flag)
         if (!test)
            return TRUE;
   }

   return 0;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{
   UINT16 *pdata;

   /* init bank structure */
   bk_init(pevent);

   /* create structured ADC0 bank */
   bk_create(pevent, "ADC0", TID_UINT16, (void **)&pdata);

   /* following code to "simulates" some ADC data */
   for (int i = 0; i < 4; i++)
      *pdata++ = rand()%1024 + rand()%1024 + rand()%1024 + rand()%1024;

   bk_close(pevent, pdata);

   /* create variable length TDC bank */
   bk_create(pevent, "TDC0", TID_UINT16, (void **)&pdata);

   /* following code to "simulates" some TDC data */
   for (int i = 0; i < 4; i++)
      *pdata++ = rand()%1024 + rand()%1024 + rand()%1024 + rand()%1024;

   bk_close(pevent, pdata);

   /* limit event rate to 100 Hz. In a real experiment remove this line */
   ss_sleep(10);

   return bk_size(pevent);
}

/*-- Periodic event ------------------------------------------------*/

INT read_periodic_event(char *pevent, INT off)
{
   UINT32 *pdata;

   /* init bank structure */
   bk_init(pevent);

   /* create SCLR bank */
   bk_create(pevent, "PRDC", TID_UINT32, (void **)&pdata);

   /* following code "simulates" some values in sine wave form */
   for (int i = 0; i < 16; i++)
      *pdata++ = 100*sin(M_PI*time(NULL)/60+i/2.0)+100;

   bk_close(pevent, pdata);

   return bk_size(pevent);
}
