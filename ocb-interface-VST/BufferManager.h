#pragma once
#include <stdexcept>
#include <cinttypes>

class BufferManager {
  public:
  BufferManager(std::size_t buffer_size, uint32_t *buffer)
    : buffer(buffer),
      size(buffer_size) {}
  
  void write(uint32_t word) {
    if (size < sizeof(word)) {
      throw std::overflow_error("BufferManager: buffer too small");
    }
    
    *buffer = word;
    buffer++;
    size -= sizeof(word);
  }
  
  void didWriteBytes(std::size_t nbytes) {
    if (nbytes % sizeof(*buffer)) {
      throw std::invalid_argument("BufferManager::didWriteBytes: Given nbytes is not a multiple of a word size");
    }
    buffer += nbytes/sizeof(*buffer);
    size -= nbytes;
  }
  
  std::size_t getSize() { return size; }
  uint32_t *getBuffer() { return buffer; }
   
  private:
  std::size_t size;
  uint32_t *buffer;
};

