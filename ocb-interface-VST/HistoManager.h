#pragma once
#include <stdexcept>
#include <cinttypes>

#define NHIST   2000
#define NBINS   100

typedef unsigned short inthis_t; // inthis_t=int_his_t, not in_this_t!
const int sizeofShort=65535;

class HistoManager {
  public:
  HistoManager(inthis_t **THistogram_temp)
    : THistogram(THistogram_temp) {}
  
  void HistoInit() {
    THistogram=new inthis_t*[NHIST];
		for(int histid=0;histid<NHIST;histid++){
			THistogram[histid]=new inthis_t[NBINS];
    }
    HistoReset();
  }

  void HistoReset() {
		for(int histid=0;histid<NHIST;histid++){
		for(int binid=0;binid<NBINS;binid++){
			THistogram[histid][binid]=0;
    }
    }
  }

  void HistoClear() {
		for(int histid=0;histid<NHIST;histid++){
			 delete [] THistogram[histid];
    }
    delete [] THistogram;
  }

  void HistoBinsSet(int HistBinsTemp[]) {
			//printf("Start the stuff %d \n", NBINS);
		for(int binid=0;binid<NBINS+1;binid++){
			EHistBins[binid]=HistBinsTemp[binid];
			//printf("EHistBins %d: %d \n",binid , EHistBins[binid]);
    }
    EBinsSet=true;
  }
  
  void InputValue(int histid, int input, int fMode){
			int binNo=HistBinning(input, fMode);
			if(binNo<0) return;
			if(THistogram[histid][binNo]>=sizeofShort) {
			   printf("Warning: InputValue in HistManager for hist %d, bin %d is larger than %d! \n", histid, binNo, sizeofShort);
				 return;
      }
			THistogram[histid][binNo]++;
      //printf("THistogram (%d, %d): %d, from %d \n", histid, binNo, THistogram[histid][binNo], input);
  }
  
  inthis_t GetValue(int histid, int binid){
			return THistogram[histid][binid];
  }
  
  // Function for the binning
  int HistBinning(int inputNo, int fMode){
 					if(!EBinsSet) {
					   printf("HistoManager::HistBinning: the EBinsSet is false, thus the binning method is not set! ");
						 printf("fMode=0 automatically. \n");
						 fMode=0;
					}
          int result=-1;
          if(inputNo<EHistBins[0] || inputNo>=EHistBins[NBINS]){
						 printf("HistoManager:: the value is beyond the scope of EHistBins! \n");
						 return result;
			    }
          switch(fMode){
             case 0:
             {
                result=inputNo%NBINS;
                break;
             }
             case 1:
             {
                for(int binid=0;binid<NBINS;binid++){
                    if(inputNo>=EHistBins[binid] && inputNo<EHistBins[binid+1]){
                            result=binid;
                            break;
                    }
                }
                break;
             }
             case 2:
             {
                int binStart=0, binEnd=NBINS, binMiddle=int(NBINS/2);
                while(binStart+1!=binEnd){
                    if(inputNo>=EHistBins[binStart] && inputNo<EHistBins[binMiddle]){
                            binEnd=binMiddle;
                    }
                    else if(inputNo>=EHistBins[binMiddle] && inputNo<EHistBins[binEnd]){
                            binStart=binMiddle;
                    }
                    binMiddle=binStart+int((binEnd-binStart)/2);
                }
                result=binStart;
                break;
             }
             default:
                printf ("Please check your fMode: %d\n", fMode);
          }
          return result;
  }

  // output the const int
  int getNhist() { return NHIST; }
  int getNbins() { return NBINS; }

   
  private:
  inthis_t **THistogram;
  bool EBinsSet=false;
  int EHistBins[NBINS+1];
};

