#include "MCB_interface.h"
#include "mcb_export.h"
#include <unistd.h>
#include <stdexcept>
#include <sstream>
#include <cstring>
#include <sys/stat.h>
#include <fcntl.h>
extern "C" {
#include "daq_interface.h"
};

using MCB::set_option_status;
using MCB::trigger_status;
using namespace std;

namespace MCB_InternalState {
}

using namespace MCB_InternalState;

// The data available flag is set 1 and the fifo storage is not empty in this case
trigger_status MCB_EXPORT MCB::get_trigger_status() {
  u_int8_t statusFIFO = get_fifo_status();
  if (statusFIFO==1) {
     return trigger_status::trigger_available;
  }
  
  return trigger_status::no_trigger;
};

// to be called at least once at the start
set_option_status MCB_EXPORT MCB::open() {
  printf("Initializing word-by-word readout implementation of MCB... ");
  bool status=enable_rw();
  if(!status) return set_option_status::error;
  
  return set_option_status::success;
}

set_option_status MCB_EXPORT MCB::close() {
  disable_rw();
  
  return set_option_status::success;
}

// to be called at the start of run
set_option_status MCB_EXPORT MCB::readout_enable() {
  return set_option_status::success;
}

// to be called at the end of a run
set_option_status MCB_EXPORT MCB::readout_disable() {
  return set_option_status::success;
}

// clear the current trigger.
set_option_status MCB_EXPORT MCB::trig_enable() {
  return set_option_status::success;
}

// set to busy status (defined but not used in FGD's DCC code).
set_option_status MCB_EXPORT MCB::trig_disable() {
  return set_option_status::success;
}

std::size_t MCB_EXPORT MCB::read_event(void *buffer, std::size_t max_buffer_size, FILE *fp) {
  // read raw event data into buffer, fail if event does not fit in max_buffer_size (throw exception or just return negative number?). return read size in bytes.
  
  char *cbuf = static_cast<char *>(buffer);
  
  int writtenBytes = 0;
  for(int ii=0;ii<4;ii++){
    // read curr line of data
   	uint32_t data = read_fifo_data(ii);

    int nbytes = sizeof(data);
    if (writtenBytes + nbytes > max_buffer_size) {
      throw overflow_error("MCB::read_event: Destination buffer not large enough.");
    }
    
    memcpy(cbuf, &data, nbytes);
    cbuf += sizeof(data);
    writtenBytes += nbytes;

    // saving data into file
    if(fp!=NULL){
       fwrite(&data, sizeof(data), 1, fp);
    }

    if(ii==3){ 
  	// move to the next event
  	fifo_next();
        return writtenBytes;
    }
  
    int statusFIFO = get_fifo_status();
    if (statusFIFO==0) {
      if(ii<3){ // fill to 4 elements for 1 event
   			data = 0x0;
        for(int jj=ii;jj<4;jj++){
        memcpy(cbuf, &data, nbytes);
        cbuf += sizeof(data);
        writtenBytes += nbytes;
        // saving data into file
        if(fp!=NULL){
           fwrite(&data, sizeof(data), 1, fp);
        }
        }
	    }
      return writtenBytes;
    }

  }


  throw runtime_error("MCB::read_event: nmaxlines reached but buffer not empty yet");
}

std::size_t MCB_EXPORT MCB::read_event_line(void *buffer, int ii, FILE *fp) {
  char *cbuf = static_cast<char *>(buffer);
  int writtenBytes = 0;
  // read curr line of data
  uint32_t data = read_fifo_data(ii);
  int nbytes = sizeof(data);
  memcpy(cbuf, &data, nbytes);
  cbuf += sizeof(data);
  writtenBytes += nbytes;
  // saving data into file
  if(fp!=NULL){
     fwrite(&data, sizeof(data), 1, fp);
  }

  return writtenBytes;
    
}

uint32_t MCB_EXPORT MCB::read_event_simple(int ii){
  uint32_t data = read_fifo_data(ii);
  return data;
}

void MCB_EXPORT MCB::GoToNextEvent(){
  fifo_next();
  return;
}

// for the following IO functions there may be multiple
// versions depending on the actual interface?
uint32_t MCB_EXPORT MCB::get_option(Option option) {
  // not implemented
  return 0;
}

set_option_status MCB_EXPORT MCB::set_option(Option option, uint32_t value) {
  return set_option_status::error;
}

