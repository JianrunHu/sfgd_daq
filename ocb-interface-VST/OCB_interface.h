#pragma once
#include <cinttypes>
#include <cstddef>
#include <stdio.h>
#include <stdlib.h>
#include "HistoManager.h"

namespace OCB {
  
  // I'm writing things c++ style, but don't really care
  // just enum or const int may be ok too
  enum class Option {
    placeholder = 0,
  };
  
  enum class set_option_status {
    error=0,
    success = 1,
  };
  
  enum class trigger_status {
    no_trigger = 0,
    trigger_available = 1,
    // ...
  };
  
  // for the following trigger related functions,
  // I think we may need multiple ones depending on the type of trigger?
  // in principle I would assume these trig_enable/disable functions are implementable using the set_option below
  
  trigger_status get_trigger_status(); // this just returns 1 if trigger is available, 0 if not, or something else for error message etc.? so maybe instead of returning int, an enum may be better (but don't really care to be honest)
  trigger_status get_fifo_status(); // this just returns 1 if fifo is not empty
  
  set_option_status open();  // to be called at the start of program
  set_option_status close(); // to be called before closing program
  
  set_option_status readout_enable();  // to be called at the start of run
  set_option_status readout_disable(); // to be called at the end of a run
  
  set_option_status trig_enable();  // clear the current trigger.
  set_option_status trig_disable(); // set to busy status (defined but not used in FGD's DCC code).
  
  std::size_t read_event(void *buffer, std::size_t max_buffer_size, FILE *fp=NULL); // read raw event data into buffer, fail if event does not fit in max_buffer_size (throw exception or just return negative number?). return read size in bytes.  
  std::size_t read_event_line(void *buffer, FILE *fp=NULL); // read one line of the raw event data into buffer. return read size in bytes.  
  uint32_t read_event_simple(); // read one line of the raw event data into buffer. return read data.  
  uint32_t read_event_histograming(HistoManager* histMana, int fMode); // for histograming, the histid should be the boardid, but I don't know
  
  // for the following IO functions there may be multiple
  // versions depending on the actual interface?
  uint32_t get_option(Option option);
  set_option_status set_option(Option option, uint32_t value);
};
