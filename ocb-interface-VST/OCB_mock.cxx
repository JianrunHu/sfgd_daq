#include "OCB_interface.h"
#include "ocb_export.h"
#include "wordutils.h"

#include <unistd.h>
#include <stdexcept>
#include <sstream>
#include <cstring>

#include <chrono>
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::seconds;
using std::chrono::system_clock;

using namespace OCB;

namespace InternalState {
  const double trigger_interval_ms = 100.;
  
  bool busy; // event is ready to be read out
  
  bool trigger_enabled;
  uint32_t last_trigger_key;
  bool did_set_last_trigger_key;
  
  std::size_t nbytesEventData;
  uint32_t *eventData;
}


using namespace InternalState;

uint32_t ss_millitime() {
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

trigger_status OCB_EXPORT OCB::get_trigger_status() {
  if (!trigger_enabled) { return trigger_status::no_trigger; }
  
  if (busy) {
    return trigger_status::trigger_available;
  }
  
  // wait for a bit, then trigger
  uint32_t curr_time = ss_millitime();
  uint32_t trigger_key = curr_time / trigger_interval_ms;
  
  if (!did_set_last_trigger_key) {
    last_trigger_key = trigger_key;
    did_set_last_trigger_key = true;
    return trigger_status::no_trigger;
  }
  
  if (last_trigger_key != trigger_key) {
    did_set_last_trigger_key = true;
    last_trigger_key = trigger_key;
    busy = true;
    return trigger_status::trigger_available;
  }
  
  return trigger_status::no_trigger;
};

// to be called at least once at the start
set_option_status OCB_EXPORT OCB::open() {
  printf("Initializing mock implementation of OCB... ");
  busy = false;
  trigger_enabled = false;
  
  last_trigger_key = 0;
  did_set_last_trigger_key = false;
  
  size_t buffer_size = 100000*sizeof(*eventData);
  eventData = new uint32_t[buffer_size/sizeof(*eventData)];
  uint32_t eventNumber = 0;
  uint32_t GTSTag = 0;
  uint32_t gateNumber = 0;
  nbytesEventData = simulateOCBEvent(buffer_size, eventData, eventNumber, GTSTag, gateNumber);

  printf("done\n");
  printf("nbytesEventData = %llu\n", nbytesEventData);
  
  return set_option_status::success;
}

set_option_status OCB_EXPORT OCB::close() {
  delete [] eventData;
  eventData = NULL;
  nbytesEventData = 0;
  
  return set_option_status::success;
}

// to be called at the start of run
set_option_status OCB_EXPORT OCB::readout_enable() {
  trig_enable();
}

// to be called at the end of a run
set_option_status OCB_EXPORT OCB::readout_disable() {
  trig_disable();
}

// clear the current trigger.
set_option_status OCB_EXPORT OCB::trig_enable() {
  trigger_enabled = true;
  busy = false;
  return set_option_status::success;
}

// set to busy status (defined but not used in FGD's DCC code).
set_option_status OCB_EXPORT OCB::trig_disable() {
  trigger_enabled = false;
  busy = true;
  return set_option_status::success;
}

void wait_for_trigger() {
  do {
    usleep(1000);
  }
  while (get_trigger_status() != trigger_status::trigger_available);
}

std::size_t OCB_EXPORT OCB::read_event(void *buffer, std::size_t max_buffer_size) {
  // read raw event data into buffer, fail if event does not fit in max_buffer_size (throw exception or just return negative number?). return read size in bytes.
  
  if (!busy) {
    // no event available
    return 0;
  }
  
  if (nbytesEventData > max_buffer_size) {
    std::stringstream ss;
    ss << "OCB::read_event destination buffer not large enough. Needed ";
    ss << nbytesEventData << " bytes, ";
    ss << "but destination buffer only had " << max_buffer_size << " bytes.";
    throw std::overflow_error(ss.str());
  }
  
  memcpy(buffer, eventData, nbytesEventData);

  return nbytesEventData;
}
  
// for the following IO functions there may be multiple
// versions depending on the actual interface?
uint32_t OCB_EXPORT OCB::get_option(Option option) {
  // not implemented
  return 0;
}

set_option_status OCB_EXPORT OCB::set_option(Option option, uint32_t value) {
  return set_option_status::error;
}
