#include "OCB_interface.h"
#include "ocb_export.h"
#include <unistd.h>
#include <stdexcept>
#include <sstream>
#include <cstring>
#include <sys/stat.h>
#include <fcntl.h>
#include "wordutils.h"
extern "C" {
#include "utility.h"
};

using OCB::set_option_status;
using OCB::trigger_status;
using namespace std;

namespace InternalState {
    int *reg_ptr = NULL;
    int mem_fd = 0; // /dev/mem memory file descriptor

    // data readout slow control arguments
    const unsigned int DATA_READOUT_START = 0x0;
    const unsigned int DATA_READOUT_STOP = 0x1;
    // number of FEBs per crate
    const int n_feb = 14;
    int slot_id[n_feb];
    // Please set the FEB channels here!!!!
    const int nFEBSet=1;
    const int slot_Choose[nFEBSet]={13};
    // encode crate ID and slot ID into board ID
    unsigned int board_id[n_feb];
    int n_board = 0;
    const int head = 0x8;
    const int cmd = 0x0;

}

using namespace InternalState;

// The data available flag is set 1 and the fifo storage is not empty in this case
trigger_status OCB_EXPORT OCB::get_trigger_status() {
  bool isEmptyData = data_av_timeout(reg_ptr);
  bool isEmptyFIFO = fifo_empty_timeout(reg_ptr, DATA_EMPTY_OFFSET);
  if (isEmptyData||isEmptyFIFO) {
    return trigger_status::no_trigger;
  }
  
  return trigger_status::trigger_available;
};

// The fifo storage is not empty in this case
trigger_status OCB_EXPORT OCB::get_fifo_status() {
  bool isEmptyFIFO = fifo_empty_timeout(reg_ptr, DATA_EMPTY_OFFSET);
  if (isEmptyFIFO) {
    return trigger_status::no_trigger;
  }
  
  return trigger_status::trigger_available;
};

// to be called at least once at the start
set_option_status OCB_EXPORT OCB::open() {
  printf("Initializing word-by-word readout implementation of OCB... ");
    /******map memory addr******/
    mem_fd = ::open("/dev/mem", O_RDWR);
    if (mem_fd == -1) {
      printf ("Error opening /dev/mem. mem_fd: 0x%x\n", mem_fd);
      return set_option_status::error;
    }
  
    //Memory map AXI register from /dev/mem to user space
    reg_ptr = static_cast<int*>(mmap(NULL, REG_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, REG_BASE));
    if (*reg_ptr == -1) {
      printf ("Error during mmap. reg_ptr: 0x%x\n", *reg_ptr);
      return set_option_status::error;
    }
  
  return set_option_status::success;
}

set_option_status OCB_EXPORT OCB::close() {
  /******unmap memory***********/
  munmap(reg_ptr, REG_SIZE);
  reg_ptr = NULL;
  
  ::close(mem_fd);
  mem_fd = 0;
  
  return set_option_status::success;
}

// to be called at the start of run
set_option_status OCB_EXPORT OCB::readout_enable() {
  // reset OCB firmware
  send_reset(reg_ptr);

  // Setting the FEB slot ID
  for (int idx=0; idx<n_feb; ++idx) slot_id[idx] = -1;
  for (int idx=0; idx<nFEBSet; ++idx) {
     int slot_temp=slot_Choose[idx];
	 	if(slot_temp<0 || slot_temp>=n_feb) continue;
	 	slot_id[slot_temp] = slot_temp;
	}
  
  // encode slot enable
  int slot_enable = 0;
  printf("Enable slot: ");
  for (int idx=0; idx<n_feb; ++idx) {
          if (slot_id[(n_feb-1)-idx] == -1) slot_enable = (slot_enable<<1) | 0;
          else {
                  printf("%d ", (n_feb-1)-idx);
                  slot_enable = (slot_enable<<1) | 1;
          }
  }
  printf("\n");

  // set enable start
  printf("Set enable start\n");
  start_data_readout(reg_ptr, slot_enable);

  // define the board_id based on the slot_id
  for (int idx=0; idx<n_feb; ++idx) {
  if (slot_id[idx] == -1) board_id[idx] = slot_id[idx];
  else {
           board_id[idx] = (CRATE_ID<<4) | slot_id[idx];
           ++n_board;
  }
  }

  // send data readout start slow control request for given FEB board IDs
  printf("Start data readout from FEB ");
  for (int idx=0; idx<n_feb; ++idx) {
          if (board_id[idx] == -1) continue;
          printf("%d ", board_id[idx]);

          // get only the 7-LSB of the board ID
          int board = get_bits(255, 0, 7); // TODO: This 255 should be changed to the coorect board_id. Here we used 255 temporarily due to hardware issue

          unsigned int word = (head<<28) | (board<<21) | (cmd<<16) | DATA_READOUT_START;
          write_slow_control(reg_ptr, word);
  }
  printf("\n");

  // read all answers from request writes
  int n = 0;
  while (!fifo_empty_timeout(reg_ptr, SLOW_CONTROL_ANSWER_EMPTY_OFFSET)) {
       int status = read_slow_control(reg_ptr);
       ++n;
  }
  if (n != n_board) {
	  printf("DATA_READOUT_START: %d slow control answers received (%d expected)\n", n, n_board);
    return set_option_status::error;
  }

  return set_option_status::success;
}

// to be called at the end of a run
set_option_status OCB_EXPORT OCB::readout_disable() {
  // send data readout stop slow control request for given FEB board IDs
  printf("Stop data readout from FEB ");
  for (int idx=0; idx<n_feb; ++idx) {
          if (board_id[idx] == -1) continue;
          printf("%d ", board_id[idx]);

          // get only the 7-LSB of the board ID
          int board = get_bits(255, 0, 7); // TODO: This 255 should be changed to the coorect board_id. Here we used 255 temporarily due to hardware issue

          unsigned int word = (head<<28) | (board<<21) | (cmd<<16) | DATA_READOUT_STOP;
          write_slow_control(reg_ptr, word);
  }
  printf("\n");

  // read all answers from request writes
  int n = 0;
  while (!fifo_empty_timeout(reg_ptr, SLOW_CONTROL_ANSWER_EMPTY_OFFSET)) {
          int status = read_slow_control(reg_ptr);
          ++n;
  }
  if (n != n_board) {
	  printf("DATA_READOUT_STOP: %d slow control answers received (%d expected)\n", n, n_board);
    return set_option_status::error;
  }

  // set enable stop
  printf("Set enable stop\n");
  stop_data_readout(reg_ptr);

  return set_option_status::success;
}

// clear the current trigger.
set_option_status OCB_EXPORT OCB::trig_enable() {
  return set_option_status::success;
}

// set to busy status (defined but not used in FGD's DCC code).
set_option_status OCB_EXPORT OCB::trig_disable() {
  return set_option_status::success;
}

std::size_t OCB_EXPORT OCB::read_event(void *buffer, std::size_t max_buffer_size, FILE *fp) {
  // read raw event data into buffer, fail if event does not fit in max_buffer_size (throw exception or just return negative number?). return read size in bytes.
  
  char *cbuf = static_cast<char *>(buffer);
  
  int writtenBytes = 0;
  while(1){
    // read curr line of data
   	uint32_t data = read_data(reg_ptr);

    int nbytes = sizeof(data);
    if (writtenBytes + nbytes > max_buffer_size) {
      throw overflow_error("OCB::read_event: Destination buffer not large enough.");
    }
    
    memcpy(cbuf, &data, nbytes);
    cbuf += sizeof(data);
    writtenBytes += nbytes;

    // saving data into file
    if(fp!=NULL){
       fwrite(&data, sizeof(data), 1, fp);
    }
  
    int headerid=isolateNbits(data, 28, 4);
    if (headerid==int(typeID::OCBEvtEnd)) {
      //printf("This should be the end of the evnet from OCB...");
      return writtenBytes;
    }
    
    bool isEmptyData = data_av_timeout(reg_ptr);
    bool isEmptyFIFO = fifo_empty_timeout(reg_ptr, DATA_EMPTY_OFFSET);
    if (isEmptyData||isEmptyFIFO) {
      return writtenBytes;
    }

  }

  throw runtime_error("OCB::read_event: nmaxlines reached but buffer not empty yet");
}

std::size_t OCB_EXPORT OCB::read_event_line(void *buffer, FILE *fp) {
  char *cbuf = static_cast<char *>(buffer);
  int writtenBytes = 0;
  // read curr line of data
  uint32_t data = read_data(reg_ptr);
  int nbytes = sizeof(data);
  memcpy(cbuf, &data, nbytes);
  cbuf += sizeof(data);
  writtenBytes += nbytes;
  // saving data into file
  if(fp!=NULL){
     fwrite(&data, sizeof(data), 1, fp);
  }

  return writtenBytes;
    
}

uint32_t OCB_EXPORT OCB::read_event_simple(){
  uint32_t data = read_data(reg_ptr);
  return data;
}

//Histogramming
uint32_t OCB_EXPORT OCB::read_event_histograming(HistoManager* histMana, int fMode) { // for histograming, the histid should be the boardid, but I don't know
  int writtenBytes = 0;
  while(1){
    // read curr line of data
   	uint32_t data = read_data(reg_ptr);
    int headerid=isolateNbits(data, 28, 4);
    if (headerid==int(typeID::OCBEvtEnd)) {
      //printf("This should be the end of the evnet from OCB...");
      return writtenBytes;
    }
    else if (headerid==int(typeID::hitAmplitude)) {
    uint32_t channelID, hitID, tagID, amplitudeID, amplitude;
    getHitAmplitude(data, channelID, hitID, tagID, amplitudeID, amplitude);

    // Histograming
    histMana->InputValue(channelID, amplitude, fMode);

    int nbytes = sizeof(data);
    writtenBytes += nbytes;
		}

/*
    // saving data into file
    if(fp!=NULL){
       fwrite(&data, sizeof(data), 1, fp);
    }
*/
  
    bool isEmptyData = data_av_timeout(reg_ptr);
    bool isEmptyFIFO = fifo_empty_timeout(reg_ptr, DATA_EMPTY_OFFSET);
    if (isEmptyData||isEmptyFIFO) {
      return writtenBytes;
    }

    
  }

  throw runtime_error("OCB::read_event: nmaxlines reached but buffer not empty yet");
}



// for the following IO functions there may be multiple
// versions depending on the actual interface?
uint32_t OCB_EXPORT OCB::get_option(Option option) {
  // not implemented
  return 0;
}

set_option_status OCB_EXPORT OCB::set_option(Option option, uint32_t value) {
  return set_option_status::error;
}

