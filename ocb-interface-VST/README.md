# OCB software interface

Compiling

```bash
# from source directory
git submodule update --init
mkdir build
cd build
cmake ..
make
```

choose implementation using `OCB_IMPL` cmake option. The default is `mock`. To switch to the `wordbyword` readout option from actual OCB (using code by Eric for the UPenn crate), use

```bash
cmake -DOCB_IMPL=wordbyword ..
make
```

explanation of programs

- `simulatewords` simulates an OCB event and writes stream of words from OCB and FEB to an output file. Run without any options to see help.
- `printwords` reads the output from an actual OCB or `simulatewords` and prints them in a human-friendly output. Run without any options to see help.
- `libOCB.so` contains an implementation of the OCB readout software interface defined in `OCB_interface.h`. Currently two implementations are provided. Amock implementation, and another implementation that supports word-by-word readout from an actual OCB (using code by Eric for the UPenn crate). They can be switched by a cmake option as mentioned above. The used implementation will be printed when executing `OCB::open`.
- `test_OCB_interface` provides an example to read out a few events from the OCB using the software interface. Run without any options to see help.
