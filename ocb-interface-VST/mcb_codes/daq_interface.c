#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>
#include "daq_interface.h"

//Macros
//Register information
#define REG_SIZE 0x20		//Size of AXI reg_bank, 32 registers
#define BASE_ADDR 0x43C00000	//Base address of AXI reg_bank
#define ENBL_OFST 0x00		//Offset to Emulator Enable Register
#define EVENT_OFST 0x10		//Offset to Event Number
#define SPILL_OFST 0x11		//Offset to Spill Number
#define TRIG_OFST 0x12		//Offset to Trig Word
#define PPS_OFST  0x13		//Offset to PPS
#define DATA_RDY_OFST 0x17 	//Offset to PL Data Ready flag
#define DATA_RCV_OFST 0x0E	//Offset to PS Ready for Data flag
//Constants
#define DATA_READ 0x01		//PL data read
#define DATA_WAIT 0x00		//Waiting for PL data

//Global Variables
volatile int *reg_ptr;		//pointer to AXI register

//Function Definitions

//Called to readout and return event#, trig word, spill, and pps
//Pass 0 to get Event #
//Pass 1 to get Trigger word
//Pass 2 to get Spill #
//Pass 3 to get PPS #
//Returns respective value
//If data cannot be returned within 1 ms, time out and return error value???
uint32_t read_fifo_data(uint8_t data_get)
{
	uint32_t data_packet = 0;
 			
	switch (data_get)
	{
		case 0:
			read_AXI(EVENT_OFST, &data_packet);
			break;
		case 1:
			read_AXI(TRIG_OFST, &data_packet);
			break;
		case 2:
			read_AXI(SPILL_OFST, &data_packet);
			break;
		case 3:
			read_AXI(PPS_OFST, &data_packet);
			break;
	}
	
	//printf("%d\n", data_packet);
	return data_packet;
}

//Check for trigger, duration 10 ms max.  Return 0 if no trigger
uint8_t get_fifo_status()
{
	uint16_t timeout_usec = 10000;
	uint16_t time_usec = 0;
	uint32_t data_ready = 0;

	while(time_usec < timeout_usec)
	{
		read_AXI(DATA_RDY_OFST, &data_ready);
		if(data_ready == 1)
		{
			return 1;
		}
		usleep(1);
		++time_usec;
	}
	return 0; 
}

//Advance fifo to the next value
void fifo_next()
{
	write_AXI(DATA_RCV_OFST, DATA_READ);

	usleep(100);

	write_AXI(DATA_RCV_OFST, DATA_WAIT);

	return;
}

//Called to initalize memory for reading
bool enable_rw()
{
	int32_t mem_fd = 0;		// /dev/mem memory file descriptor

	mem_fd = open("/dev/mem", O_RDWR);
	if(mem_fd == -1)
	{
		printf("Error! mem_fd: 0x%x \n", mem_fd);
    return false;
	}

	printf("Memory mapping AXI register from /dev/mem to user space. \n");
	reg_ptr = mmap(NULL, REG_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, BASE_ADDR);
	if(*reg_ptr == -1)
	{
		printf("Error! reg_ptr: 0x%x \n", *reg_ptr);
    return false;
	}
	return true;
}

//Called to close memory
bool disable_rw()
{
	printf("Unmapping memory. \n");
	munmap(reg_ptr, REG_SIZE);
	return true;
}

//Called internally to read data
void read_AXI(uint16_t offset, uint32_t *output)
{
	//Read Reg: var = *(reg_ptr + reg_offset)

	*output = *(reg_ptr + offset);
	return;
}

//Called internally to write data
void write_AXI(uint16_t offset, uint32_t data) 
{
	//Write Reg: *(reg_ptr + reg_offset) = data
	
	*(reg_ptr + offset) = data;
	return;
}


