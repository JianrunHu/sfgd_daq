#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <stdbool.h>
#include <stdint.h>

//Function declarations
//Open / Close memory pointer to registers
bool enable_rw();
bool disable_rw();

//Pass reg offset value, 0-31, and provide a variable to pass by ref
void read_AXI(uint16_t offset, uint32_t *output);
//Pass reg offset value, 0-31, and provide data to write to reg
void write_AXI(uint16_t offset, uint32_t data);

//Pass 0, 1, 2, or 3 to get a specific piece of data from the current fifo index 
//0 is Event #
//1 is Trigger word
//2 is Spill #
//3 is PPS
uint32_t read_fifo_data(uint8_t data_get);

//Get status of fifo, return 1 if data ready, return 0 if not ready after 1 ms
uint8_t get_fifo_status();

//Run to indicate to the PL that we are done with the current fifo index, go to next
void fifo_next();

//Need to add FIFO reset function
