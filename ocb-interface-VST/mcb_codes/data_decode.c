#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sys/ioctl.h>
#include <time.h>

// number of FEBs per crate
const int n_feb = 14;

// @brief get number with a given range of bits
// @param start_bit: LSB
// @param n_bits: (MSB - LSB) + 1
int get_bits(int word, int start_bit, int n_bits) {
  unsigned int mask = ((1 << n_bits) - 1);
  return (word >> start_bit) & mask;
}

// @brief read data when data is available
// @param argv[1]: binary file to decode
int main (int argc, char *argv[]) {
	if (argc < 2) {
		printf("Binary file to decode not given!\n");
		return -1;
	}
	else if (argc == 2) {
		if (strstr(argv[1], ".bin") == NULL) {
      printf("Input file must be a .bin file!\n");
      return -1;
    }
	}
	else {
		printf("Invalid number of arguments!\n");
		return -1;
	}

	char filename[80];
  strcpy(filename, argv[1]);
  char txtname[80];
  strcpy(txtname, argv[1]);
  char *pch = strstr(txtname, ".bin");
  strncpy(pch, ".txt", 4);

  // open file
  FILE *read_fp, *write_fp;
  int buflen = 4;
  unsigned char currline[buflen];
  read_fp = fopen(filename, "rb");
  write_fp = fopen(txtname, "w");

	if (read_fp == NULL) {
    printf("Error opening file %s\n", filename);
    return -1;
  }
  if (write_fp == NULL) {
    printf("Error opening file %s\n", txtname);
    return -1;
  }

  uint32_t word;
  int ii=0;
  while (fread(&word, sizeof(word), 1, read_fp)) {
    if (feof(read_fp)) break;

		printf("\rDecoding %s ...", filename);
    fflush(stdout);

    int  number= get_bits(word, 0, 32);
    fprintf(write_fp, "Data:0x%08X\t type: %d, number: %d\r\n", word, ii, number);
    ii++;
    if(ii==4) ii=0;
  }

	printf("\n");

  // close file
  fclose(read_fp);
  fclose(write_fp);

  printf("%s decoded!\n", filename);

	return 0;
}
