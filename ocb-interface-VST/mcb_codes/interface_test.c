#include "daq_interface.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//Simple prog to test initial	
int main()
{
	enable_rw();
	
	while(1)
	{
		if(get_fifo_status() == 1)
		{
			for(int i = 0; i < 4; i++)
			{
				read_fifo_data(i);
			}
			fifo_next();
			
			disable_rw();

			return 0;
		}
	}
        
	disable_rw();

	return 0;
}

