#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sys/ioctl.h>
#include <time.h>

// number of FEBs per crate
const int n_feb = 14;

// @brief get number with a given range of bits
// @param start_bit: LSB
// @param n_bits: (MSB - LSB) + 1
int get_bits(int word, int start_bit, int n_bits) {
  unsigned int mask = ((1 << n_bits) - 1);
  return (word >> start_bit) & mask;
}

// @brief read data when data is available
// @param argv[1]: binary file to decode
int main (int argc, char *argv[]) {
	if (argc < 2) {
		printf("Binary file to decode not given!\n");
		return -1;
	}
	else if (argc == 2) {
		if (strstr(argv[1], ".bin") == NULL) {
      printf("Input file must be a .bin file!\n");
      return -1;
    }
	}
	else {
		printf("Invalid number of arguments!\n");
		return -1;
	}

	char filename[80];
  strcpy(filename, argv[1]);
  char txtname[80];
  strcpy(txtname, argv[1]);
  char *pch = strstr(txtname, ".bin");
  strncpy(pch, ".txt", 4);

  // open file
  FILE *read_fp, *write_fp;
  int buflen = 4;
  unsigned char currline[buflen];
  read_fp = fopen(filename, "rb");
  write_fp = fopen(txtname, "w");

	if (read_fp == NULL) {
    printf("Error opening file %s\n", filename);
    return -1;
  }
  if (write_fp == NULL) {
    printf("Error opening file %s\n", txtname);
    return -1;
  }

  uint32_t word;
  while (fread(&word, sizeof(word), 1, read_fp)) {
    if (feof(read_fp)) break;

		printf("\rDecoding %s ...", filename);
    fflush(stdout);

		int id = get_bits(word, 28, 4);
    if (id==8) {
      int gate_type = get_bits(word, 25, 3);
      int gate_tag = get_bits(word, 23, 2);
      int event_number = get_bits(word, 0, 23);
	    fprintf(write_fp, "Data:0x%08X\tSYNC Word\tGate type:%d\tGate tag:%d\tEvent number:%d\r\n", word, gate_type, gate_tag, event_number);
	  }
	  else if (id==9) {
			if (get_bits(word, 0, 16) == 0) fprintf(write_fp, "Data:0x%08X\tOCB Trailer Word No error\r\n", word);
			else {
				if (get_bits(word, 15, 1) == 1) fprintf(write_fp, "Data:0x%08X\tOCB Trailer Word\tGate open timeout\r\n", word);
				if (get_bits(word, 14, 1) == 1) fprintf(write_fp, "Data:0x%08X\tOCB Trailer Word\tGate close error\r\n", word);
		     for (int i=0; i<n_feb; ++i) {
					if (get_bits(word, 13-i, 1) == 1) fprintf(write_fp, "Data:0x%08X\tOCB Trailer Word\tDAQ unit %d error\r\n", word, 13-i);
		   	}
			}
	    fprintf(write_fp, "\r\n");
    }
    else {
      if (id==0 || id==6) {
				int board = get_bits(word, 20, 8);
        int b19 = get_bits(word, 19, 1);
        int gate_type = get_bits(word, 16, 3);
        int gate_number = get_bits(word, 0, 16);
        int gate_time = get_bits(word, 0, 11);
        if (id == 0) {
            if (b19 == 0) fprintf(write_fp, "Data:0x%08X\tGate Header [0]\tBoard:%d\tGate type:%d\tGate number:%d\r\n", word, board, gate_type, gate_number);
            else fprintf(write_fp, "Data:0x%08X\tGate Header [1]\tBoard:%d\tGate time from GTS:%d\r\n", word, board, gate_time);
        }
        else fprintf(write_fp, "Data:0x%08X\tGate Trailer\tBoard:%d\tGate type:%d\tGate number:%d\r\n", word, board, gate_type, gate_number);
      }
      else if (id==1 || id==4) {
        int tag = get_bits(word, 0, 28);
        int b1_0 = get_bits(word, 0, 2);
        if (id == 1) fprintf(write_fp, "Data:0x%08X\tGTS Header\tGTS Tag:%d\tTag ID:%d\r\n", word, tag, b1_0);
        else fprintf(write_fp, "Data:0x%08X\tGTS Trailer 1\tGTS Tag:%d\tTag ID:%d\r\n", word, tag, b1_0);
      }
			else if (id==2 || id==3) {
        int channel = get_bits(word, 20, 8);
        int hit = get_bits(word, 17, 3);
        int tag = get_bits(word, 15, 2);
        int edge = get_bits(word, 14, 1);
        int amplitude = get_bits(word, 12, 3);
				int amp_value = get_bits(word, 0, 12);
				int time_value = get_bits(word, 0, 14);
        if (id == 2) {
          if (edge == 0) fprintf(write_fp, "Data:0x%08X\tHit Time:%d\tChannel:%d\tHit ID:%d\tTag ID:%d\tEdge:Rising\r\n", word, time_value, channel, hit, tag);
          else fprintf(write_fp, "Data:0x%08X\tHit Time:%d\tChannel:%d\tHit ID:%d\tTag ID:%d\tEdge:Falling\r\n", word, time_value, channel, hit, tag);
        }
        else {
          if (amplitude == 2) fprintf(write_fp, "Data:0x%08X\tHit Amplitude:%d\tChannel:%d\tHit ID:%d\tTag ID:%d\tAmplitude:HG\r\n", word, amp_value, channel, hit, tag);
          else if (amplitude == 3) fprintf(write_fp, "Data:0x%08X\tHit Amplitude:%d\tChannel:%d\tHit ID:%d\tTag ID:%d\tAmplitude:LG\r\n", word, amp_value,  channel, hit, tag);
        }
      }
      else if (id == 5) {
        int gts_time = get_bits(word, 0, 20);
        fprintf(write_fp, "Data:0x%08X\tGTS Trailer 2\tGTS Time:%d\r\n", word, gts_time);
      }
      else if (id == 7) {
        int gate_time = get_bits(word, 0, 28);
        fprintf(write_fp, "Data:0x%08X\tGate Time:%d\r\n", word, gate_time);
      }
      else if (id == 11) {
				int board = get_bits(word, 20, 8);
        int b19 = get_bits(word, 19, 1);
        int hold_time = get_bits(word, 0, 11);
        if (b19 == 0) fprintf(write_fp, "Data:0x%08X\tHold Time [0]\tBoard:%d\tHold Start Time:%d\r\n", word, board, hold_time);
        else fprintf(write_fp, "Data:0x%08X\tHold Time [1]\tBoard:%d\tHold Stop Time:%d\r\n", word, board, hold_time);
      }
			else if (id == 13) {
				int n_error = get_bits(word, 0, 16);
				if (get_bits(word, 16, 1) == 1) fprintf(write_fp, "Data:0x%08X\tFEB Trailer Word\tEvent done timeout\r\n", word);
				fprintf(write_fp, "Data:0x%08X\tFEB Trailer Word\tNumber of decoder errors:%d\r\n", word, n_error);
			}
			else if (id == 15) {
				int board = get_bits(word, 20, 8);
        int param = get_bits(word, 16, 4);
        if (param == 1) {
          fprintf(write_fp, "Data:0x%08X\tSpecial Word\tBoard:%d\tReadout end\r\n", word, board);
        }
        else if (param == 2) {
          fprintf(write_fp, "Data:0x%08X\tSpecial Word\tBoard:%d\tGTS tag reset\r\n", word, board);
        }
        else if (param == 4) {
          fprintf(write_fp, "Data:0x%08X\tSpecial Word\tBoard:%d\tGate number reset\r\n", word, board);
        }
        else if (param == 6) {
          fprintf(write_fp, "Data:0x%08X\tSpecial Word\tBoard:%d\tGTS tag + gate number reset\r\n", word, board);
        }
        else if (param == 0) {
					if (get_bits(word, 5, 1) == 1) fprintf(write_fp, "Data:0x%08X\tSpecial Word	Board:%d	L0 FIFO full\n", word, board);
					if (get_bits(word, 4, 1) == 1) fprintf(write_fp, "Data:0x%08X\tSpecial Word	Board:%d	L1 FIFO full\n", word, board);
					if (get_bits(word, 3, 1) == 1) fprintf(write_fp, "Data:0x%08X\tSpecial Word	Board:%d	L2 FIFO full\n", word, board);
					if (get_bits(word, 2, 1) == 1) fprintf(write_fp, "Data:0x%08X\tSpecial Word	Board:%d	GTS lost\n", word, board);
					if (get_bits(word, 1, 1) == 1) fprintf(write_fp, "Data:0x%08X\tSpecial Word	Board:%d	SYNC decoder error\n", word, board);
					if (get_bits(word, 0, 1) == 1) fprintf(write_fp, "Data:0x%08X\tSpecial Word	Board:%d	Ext clk error\n", word, board);
        }
			}
    }
	}
	printf("\n");

  // close file
  fclose(read_fp);
  fclose(write_fp);

  printf("%s decoded!\n", filename);

	return 0;
}
