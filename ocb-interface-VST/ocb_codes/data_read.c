#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sys/ioctl.h>
#include <time.h>
#include "utility.h"

// data readout slow control arguments
const unsigned int DATA_READOUT_START = 0x0;
const unsigned int DATA_READOUT_STOP = 0x1;

// number of FEBs per crate
const int n_feb = 14;

// @brief read data when data is available and write to binary file
// @param argv[1]: output binary file name
// @param argv[2]: binary file size limit
// @param argv[3] to argv[16]: FEB slot ID to read data from
int main (int argc, char *argv[]) {
	float filesize;
	int slot_id[14];
	for (int idx=0; idx<n_feb; ++idx) slot_id[idx] = -1;
	if (argc < 2) {
		printf("Output binary file name not given!\n");
		return -1;
	}
	else if (argc == 2) {
		if (strstr(argv[1], ".bin") == NULL) {
      printf("Input file must be a .bin file!\n");
      return -1;
    }
		printf("Insert file size limit in MB: ");
		scanf("%f", &filesize);
		printf("Insert FEB slot ID to read data from in increasing order separated by whitespace: ");
		int idx=0;
		char temp;
		do {
	       scanf("%d%c", &slot_id[atoi(argv[idx])], &temp); 
	       ++idx;
	  } while(temp != '\n'); 
	}
	else if ((argc>2) && (argc<18)) {
		if (strstr(argv[1], ".bin") == NULL) {
      printf("Input file must be a .bin file!\n");
      return -1;
    }
		filesize = atof(argv[2]);
		for (int idx=3; idx<argc; ++idx) slot_id[atoi(argv[idx])] = atoi(argv[idx]);
	}
	else {
		printf("Invalid number of argumenst!\n");
		return -1;
	}

  //for (int idx=0; idx<n_feb; ++idx) {
	//	printf("slot_id %d: %d \n", idx, slot_id[idx]);
  //}
	// encode crate ID and slot ID into board ID
  unsigned int board_id[n_feb];
	int n_board = 0;
  for (int idx=0; idx<n_feb; ++idx) {
    if (slot_id[idx] == -1) board_id[idx] = slot_id[idx];
    else {
			board_id[idx] = (CRATE_ID<<4) | slot_id[idx];
			++n_board;
		}
		//printf("board_id %d: %d ", idx, board_id[idx]);
  }

	// map memory address
	int *reg_ptr;
	int mem_fd = 0; // /dev/mem memory file descriptor

	// open /dev/mem
	mem_fd = open("/dev/mem", O_RDWR);
	if (mem_fd == -1) {
		printf("Error opening /dev/mem. mem_fd: 0x%x\n", mem_fd);
		return -1;
	}

	// memory map AXI register from /dev/mem to user space
	//reg_ptr = mmap(NULL, REG_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, REG_BASE);
	reg_ptr = (int*)mmap(NULL, REG_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, mem_fd, REG_BASE);
	if (*reg_ptr == -1) {
		printf("Error during mmap. reg_ptr: 0x%x\n", *reg_ptr);
		return -1;	
	}
	printf(" reg_ptr: 0x%x\n", *reg_ptr);

	// open file
  FILE *fp;
  int buflen = 20;
  char currline[buflen];  
	char *filename = argv[1];
	fp = fopen(filename, "wb");		
  if (fp == NULL) {
  	printf("Error opening file %s\n", filename);
		return -1;	
  }

	// reset OCB firmware
  send_reset(reg_ptr); 

	// encode slot enable
	int slot_enable = 0;
	printf("Enable slot: ");
	for (int idx=0; idx<n_feb; ++idx) {
		if (slot_id[(n_feb-1)-idx] == -1) slot_enable = (slot_enable<<1) | 0;
		else {
			printf("%d ", (n_feb-1)-idx);
			slot_enable = (slot_enable<<1) | 1;
		}
		//printf("slot_enable: %d ", slot_enable);
	}
	printf("\n");

	// set enable start
	printf("Set enable start\n");
	start_data_readout(reg_ptr, slot_enable);

	const int head = 0x8;
	const int cmd = 0x0;
	unsigned int word;
	int status;

	// send data readout start slow control request for given FEB board IDs
	printf("Start data readout from FEB ");
	for (int idx=0; idx<n_feb; ++idx) {
		if (board_id[idx] == -1) continue;
		printf("%d ", board_id[idx]);

		// get only the 7-LSB of the board ID
		int board = get_bits(255, 0, 7);

		word = (head<<28) | (board<<21) | (cmd<<16) | DATA_READOUT_START;
	  printf("word is set here: %d, %d, %d, %d, %d\n", head, board, cmd, DATA_READOUT_START, word);
		write_slow_control(reg_ptr, word);
	}
	printf("\n");

	// read all answers from request writes
	int n = 0;
   //printf("reg_ptr: 0x%x\n", *reg_ptr);
   //printf(" Slow control!!!:  %d, %d  \n", SLOW_CONTROL_ANSWER_EMPTY_OFFSET, !fifo_empty_timeout(reg_ptr, SLOW_CONTROL_ANSWER_EMPTY_OFFSET) );
    while (!fifo_empty_timeout(reg_ptr, SLOW_CONTROL_ANSWER_EMPTY_OFFSET)) {
		status = read_slow_control(reg_ptr);
		++n;
	}
	if (n != n_board) printf("DATA_READOUT_START: %d slow control answers received (%d expected)\n", n, n_board);

	int special_word_empty = read_reg(reg_ptr, SPECIAL_WORD_EMPTY_OFFSET);

	// write data to output binary file
	filesize *= 1e6;
	float currsize = 0;
	while ((currsize<filesize) && !data_av_timeout(reg_ptr)) {
  	while (!fifo_empty_timeout(reg_ptr, DATA_EMPTY_OFFSET)) {
			printf("\rProgress: %.2f KB of %.2f MB written ...", currsize/1e3, filesize/1e6);
			fflush(stdout);

			uint32_t data = read_data(reg_ptr);
			fwrite(&data, sizeof(data), 1, fp);
			currsize += 4;

			// read special word if any
			special_word_empty = read_reg(reg_ptr, SPECIAL_WORD_EMPTY_OFFSET);
			if (!special_word_empty) {
				int special_word = read_special_word(reg_ptr);
				int board = get_bits(special_word, 20, 8);
				int id = get_bits(special_word, 0, 20);
				if (id == 0x10000) printf("FEB %d: Readout end\n", board);
				else if (id == 0x20000) printf("FEB %d: GTS tag reset\n", board);
				else if (id == 0x40000) printf("FEB %d: Gate number reset\n", board);
				else if (id == 0x60000) printf("FEB %d: GTS tag + gate number reset\n", board);
				else if (id == 0x0FFFF) {
					if (get_bits(special_word, 5, 1) == 1) printf("FEB %d: L0 FIFO full\n", board);
					if (get_bits(special_word, 4, 1) == 1) printf("FEB %d: L1 FIFO full\n", board);
					if (get_bits(special_word, 3, 1) == 1) printf("FEB %d: L2 FIFO full\n", board);
					if (get_bits(special_word, 2, 1) == 1) printf("FEB %d: GTS lost\n", board);
					if (get_bits(special_word, 1, 1) == 1) printf("FEB %d: SYNC decoder error\n", board);
					if (get_bits(special_word, 0, 1) == 1) printf("FEB %d: Ext clk error\n", board);
				}
			}
		}
	}
	printf("\n");
	if (currsize < filesize) printf("Timeout for data available flag!\n");
	else printf("File size limit reached!\n");

	// send data readout stop slow control request for given FEB board IDs
	printf("Stop data readout from FEB ");
	for (int idx=0; idx<n_feb; ++idx) {
		if (board_id[idx] == -1) continue;
		printf("%d ", board_id[idx]);

		// get only the 7-LSB of the board ID
		int board = get_bits(255, 0, 7);

		word = (head<<28) | (board<<21) | (cmd<<16) | DATA_READOUT_STOP;
		write_slow_control(reg_ptr, word);
	}
	printf("\n");

	// read all answers from request writes
	n = 0;
  while (!fifo_empty_timeout(reg_ptr, SLOW_CONTROL_ANSWER_EMPTY_OFFSET)) {
		status = read_slow_control(reg_ptr);
		++n;
	}
	if (n != n_board) printf("DATA_READOUT_STOP: %d slow control answers received (%d expected)\n", n, n_board);

	// set enable stop
	printf("Set enable stop\n");
	stop_data_readout(reg_ptr);

	// write remaining data in FIFO (if any) to output binary file
	printf("Write remaining data (if any) to output file ...\n");
	while (!data_av_timeout(reg_ptr)) {
  	while (!fifo_empty_timeout(reg_ptr, DATA_EMPTY_OFFSET)) {
			int data = read_data(reg_ptr);
			fwrite(&data, sizeof(data), 1, fp);
		}
	}
	printf("Timeout for data available flag!\n");

	// read special word if any
	printf("Read remaining special word (if any) ...\n");
  while (!fifo_empty_timeout(reg_ptr, SPECIAL_WORD_EMPTY_OFFSET)) {
		int special_word = read_special_word(reg_ptr);
		int board = get_bits(special_word, 20, 8);
		int id = get_bits(special_word, 0, 20);
		if (id == 0x10000) printf("FEB %d: Readout end\n", board);
		else if (id == 0x20000) printf("FEB %d: GTS tag reset\n", board);
		else if (id == 0x40000) printf("FEB %d: Gate number reset\n", board);
		else if (id == 0x60000) printf("FEB %d: GTS tag + gate number reset\n", board);
		else if (id == 0x0FFFF) {
			if (get_bits(special_word, 5, 1) == 1) printf("FEB %d: L0 FIFO full\n", board);
			if (get_bits(special_word, 4, 1) == 1) printf("FEB %d: L1 FIFO full\n", board);
			if (get_bits(special_word, 3, 1) == 1) printf("FEB %d: L2 FIFO full\n", board);
			if (get_bits(special_word, 2, 1) == 1) printf("FEB %d: GTS lost\n", board);
			if (get_bits(special_word, 1, 1) == 1) printf("FEB %d: SYNC decoder error\n", board);
			if (get_bits(special_word, 0, 1) == 1) printf("FEB %d: Ext clk error\n", board);
		}
	}
	printf("Timeout for special word FIFO empty!\n");

	// memory unmap AXI register from /dev/mem to user space
	munmap(reg_ptr, REG_SIZE);
 
  // close /dev/mem
  close(mem_fd);

	// close file
  fclose(fp);

	return 0;
}
