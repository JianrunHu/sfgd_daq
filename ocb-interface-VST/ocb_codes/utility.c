#include "utility.h"

const int REG_BASE = 0x43C00000; // pointer to AXI register (from Vivado)
const int REG_SIZE = 48;

const int RESET_OFFSET = 0;

const int DATA_ENABLE_OFFSET = 4;
const int DATA_ENABLE_IDLE_OFFSET = 38;

const int TRIGGER_CONFIG_OFFSET = 5;
const int TRIGGER_ENABLE_OFFSET = 6;
const int TRIGGER_CONFIG_IDLE_OFFSET = 41;

const int SLOW_CONTROL_REQUEST_ALMOST_FULL_OFFSET = 40;
const int SLOW_CONTROL_REQUEST_OFFSET = 27;
const int SLOW_CONTROL_REQUEST_WRITE_OFFSET = 28;
const int SLOW_CONTROL_ANSWER_EMPTY_OFFSET = 41;
const int SLOW_CONTROL_ANSWER_OFFSET = 42;
const int SLOW_CONTROL_ANSWER_READ_NEXT_OFFSET = 29;

const int DATA_AVAILABLE_OFFSET = 43;
const int DATA_EMPTY_OFFSET = 44;
const int DATA_OFFSET = 45;
const int DATA_READ_NEXT_OFFSET = 30;

const int SPECIAL_WORD_EMPTY_OFFSET = 46;
const int SPECIAL_WORD_OFFSET = 47;
const int SPECIAL_WORD_READ_NEXT_OFFSET = 31;

int CRATE_ID = 15;

// @brief get number with a given range of bits
int get_bits(int word, int start_bit, int n_bits) {
	unsigned int mask = ((1 << n_bits) - 1);
	return (word >> start_bit) & mask;
}

// @brief read from register with valid offset
int read_reg(volatile int *reg_ptr, int reg_offset) {
	//printf("Calling the read_reg in the code! \n");
	if (reg_offset<0 || reg_offset>63) {
		printf("Register address offset to read from is out of range!\n");
		return -1;
	}

	return *(reg_ptr+reg_offset);
}

// @brief write to register with valid offset
int write_reg(volatile int *reg_ptr, int reg_offset, unsigned int reg_data){
	if (reg_offset<0 || reg_offset>31) {
		printf("Register address offset to write to is out of range!\n");
		return -1;
	}

	*(volatile int*)(reg_ptr+reg_offset) = reg_data;  // add "(colatile int*)" to avoid the optimization through "-O2"

	return *(reg_ptr+reg_offset);
}

// @brief send reset pulse
int send_reset(int *reg_ptr) {
  write_reg(reg_ptr, RESET_OFFSET, 0x1);
	usleep(1000);
	write_reg(reg_ptr, RESET_OFFSET, 0x0);

	return 0;
}

// @brief start data readout from given slot enables
int start_data_readout(int *reg_ptr, int slot_enable) {
	if (read_reg(reg_ptr, DATA_ENABLE_IDLE_OFFSET) == 0) {
		int enable = (1<<14) | slot_enable;
		write_reg(reg_ptr, DATA_ENABLE_OFFSET, enable);
	}
	else {
		printf("Data readout start cannot be set now! Try again later.\n");
		return -1;
	}

	return 0;
}

// @brief stop data readout
int stop_data_readout(int *reg_ptr) {
	if (read_reg(reg_ptr, DATA_ENABLE_IDLE_OFFSET) == 0) write_reg(reg_ptr, DATA_ENABLE_OFFSET, 0);
	else {
		printf("Data readout stop cannot be set now! Try again later.\n");
		return -1;
	}

	return 0;
}

// @brief waits until FIFO is not empty
int fifo_empty_timeout(int *reg_ptr, int fifo_empty_offset) {
	const int timeout_usec = 1000; // 1ms
	int time_usec = 0;

	int fifo_empty = read_reg(reg_ptr, fifo_empty_offset);
   while (fifo_empty) {
	if (time_usec > timeout_usec) return 1;
		usleep(1);
		++time_usec;

    fifo_empty = read_reg(reg_ptr, fifo_empty_offset);
  }
  //printf("fifo_empty_timeout: %d \n", fifo_empty);

  return fifo_empty;
}

// @brief write slow control request
int write_slow_control(int *reg_ptr, unsigned int request) {
//  printf("utility.c: Writing the slow control now...");
	// wait until FIFO is not almost full
	int fifo_almost_full = read_reg(reg_ptr, SLOW_CONTROL_REQUEST_ALMOST_FULL_OFFSET);
	while (fifo_almost_full) {
			 printf("Slow control request FIFO almost full!\n");
			 usleep(1);
	     fifo_almost_full = read_reg(reg_ptr, SLOW_CONTROL_REQUEST_ALMOST_FULL_OFFSET);
  }

	int status = write_reg(reg_ptr, SLOW_CONTROL_REQUEST_OFFSET, request);

	// assert write to FIFO and de-assert right after
	write_reg(reg_ptr, SLOW_CONTROL_REQUEST_WRITE_OFFSET, 0x1);
	write_reg(reg_ptr, SLOW_CONTROL_REQUEST_WRITE_OFFSET, 0x0);

	return status;
}

// @brief read slow control answer
int read_slow_control(int *reg_ptr) {
	int status = read_reg(reg_ptr, SLOW_CONTROL_ANSWER_OFFSET);

	// assert read next from FIFO and de-assert right after
	write_reg(reg_ptr, SLOW_CONTROL_ANSWER_READ_NEXT_OFFSET, 0x1);
	write_reg(reg_ptr, SLOW_CONTROL_ANSWER_READ_NEXT_OFFSET, 0x0);

	if (get_bits(status, 28, 4) == 0xE) {
		printf("FEB slow control error: 0x%08X\n", status);
		return -1;
	}
	else if (get_bits(status, 28, 4) > 0xA) {
		printf("OCB slow control error: 0x%08X\n", status);
		return -1;
	}

	return status;
}

// @brief write slow control request and read slow control answer right after
int write_read_slow_control(int *reg_ptr, unsigned int request) {
	// wait until FIFO is not almost full
	int fifo_almost_full = read_reg(reg_ptr, SLOW_CONTROL_REQUEST_ALMOST_FULL_OFFSET);
	while (fifo_almost_full) printf("Slow control request FIFO almost full!\n");

	int status = write_reg(reg_ptr, SLOW_CONTROL_REQUEST_OFFSET, request);

	// assert write to FIFO and de-assert right after
	write_reg(reg_ptr, SLOW_CONTROL_REQUEST_WRITE_OFFSET, 0x1);
	write_reg(reg_ptr, SLOW_CONTROL_REQUEST_WRITE_OFFSET, 0x0);

	// wait until FIFO is not empty
   	if (fifo_empty_timeout(reg_ptr, SLOW_CONTROL_ANSWER_EMPTY_OFFSET)) {
		printf("Slow control answer FIFO not empty timeout!\n");
		return -1;
	}
	
	status = read_reg(reg_ptr, SLOW_CONTROL_ANSWER_OFFSET);

	// assert read next from FIFO and de-assert right after
	write_reg(reg_ptr, SLOW_CONTROL_ANSWER_READ_NEXT_OFFSET, 0x1);
	write_reg(reg_ptr, SLOW_CONTROL_ANSWER_READ_NEXT_OFFSET, 0x0);

	if (get_bits(status, 28, 4) == 0xE) {
		printf("FEB slow control error: 0x%08X\n", status);
		return -1;
	}
	else if (get_bits(status, 28, 4) > 0xA) {
		printf("OCB slow control error: 0x%08X\n", status);
		return -1;
	}

	return status;
}

// @brief waits until data is available
int data_av_timeout(int *reg_ptr) {
	const int timeout_msec = 3000; // 3s
	int time_msec = 0;

	int data_av = read_reg(reg_ptr, DATA_AVAILABLE_OFFSET);
  while (!data_av) {
		if (time_msec > timeout_msec) return 1;
		usleep(1000);
		++time_msec;

     data_av = read_reg(reg_ptr, DATA_AVAILABLE_OFFSET);
   }

   return 0;
}

// @brief read data
uint32_t read_data(int *reg_ptr) {
	uint32_t data = read_reg(reg_ptr, DATA_OFFSET);

	// assert read next from FIFO and de-assert right after
	write_reg(reg_ptr, DATA_READ_NEXT_OFFSET, 0x1);
	//usleep(1);
	write_reg(reg_ptr, DATA_READ_NEXT_OFFSET, 0x0);

	return data;
}

// @brief read special_word
int read_special_word(int *reg_ptr) {
	int special_word = read_reg(reg_ptr, SPECIAL_WORD_OFFSET);

	// assert read next from FIFO and de-assert right after
	write_reg(reg_ptr, SPECIAL_WORD_READ_NEXT_OFFSET, 0x1);
	//usleep(1);
	write_reg(reg_ptr, SPECIAL_WORD_READ_NEXT_OFFSET, 0x0);

	return special_word;
}

// @brief set trigger config
int set_trigger_config(int *reg_ptr, int trigger_config) {
	if (read_reg(reg_ptr, TRIGGER_CONFIG_IDLE_OFFSET) == 1) {
		if (trigger_config == 0) printf("Warning: Trigger config is set to all '0'!\n");
		int config_send = (1<<31) | trigger_config;
		write_reg(reg_ptr, TRIGGER_CONFIG_OFFSET, config_send);
		write_reg(reg_ptr, TRIGGER_CONFIG_OFFSET, 0x0);
	}
	else if (read_reg(reg_ptr, TRIGGER_ENABLE_OFFSET) == 1) {
		printf("Disable trigger matching first before setting trigger config!\n");
		return -1;
	}
	else {
		printf("Trigger config cannot be set now! Try again later.\n");
		return -1;
	}

	return 0;
}

// @brief enable or disable trigger matching
int enable_trigger_match(int *reg_ptr, bool trigger_enable) {
  write_reg(reg_ptr, TRIGGER_ENABLE_OFFSET, trigger_enable);

	return 0;
}

