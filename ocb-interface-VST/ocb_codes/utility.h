#pragma once
#ifndef OCB_SOFTWARE_H
#define OCB_SOFTWARE_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <time.h>

extern const int REG_BASE; // pointer to AXI register (from Vivado)
extern const int REG_SIZE;

extern const int RESET_OFFSET;

extern const int DATA_ENABLE_OFFSET;
extern const int DATA_ENABLE_IDLE_OFFSET;

extern const int TRIGGER_CONFIG_OFFSET;
extern const int TRIGGER_ENABLE_OFFSET;
extern const int TRIGGER_CONFIG_IDLE_OFFSET;

extern const int SLOW_CONTROL_REQUEST_ALMOST_FULL_OFFSET;
extern const int SLOW_CONTROL_REQUEST_OFFSET;
extern const int SLOW_CONTROL_REQUEST_WRITE_OFFSET;
extern const int SLOW_CONTROL_ANSWER_EMPTY_OFFSET;
extern const int SLOW_CONTROL_ANSWER_OFFSET;
extern const int SLOW_CONTROL_ANSWER_READ_NEXT_OFFSET;

extern const int DATA_AVAILABLE_OFFSET;
extern const int DATA_EMPTY_OFFSET;
extern const int DATA_OFFSET;
extern const int DATA_READ_NEXT_OFFSET;

extern const int SPECIAL_WORD_EMPTY_OFFSET;
extern const int SPECIAL_WORD_OFFSET;
extern const int SPECIAL_WORD_READ_NEXT_OFFSET;

extern int CRATE_ID;

int get_bits(int word, int start_bit, int n_bits);

int read_reg(volatile int *reg_ptr, int reg_offset);
int write_reg(volatile int *reg_ptr, int reg_offset, unsigned int reg_data);

int send_reset(int *reg_ptr);

int start_data_readout(int *reg_ptr, int slot_enable);
int stop_data_readout(int *reg_ptr);

int fifo_empty_timeout(int *reg_ptr, int offset);

int write_slow_control(int *reg_ptr, unsigned int request);
int read_slow_control(int *reg_ptr);
int write_read_slow_control(int *reg_ptr, unsigned int request);

int data_av_timeout(int *reg_ptr);

uint32_t read_data(int *reg_ptr);

int read_special_word(int *reg_ptr);

int set_trigger_config(int *reg_ptr, int trigger_config);
int enable_trigger_match(int *reg_ptr, bool trigger_enable);

#endif
