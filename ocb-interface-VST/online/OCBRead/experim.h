/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Tue May  9 06:43:33 2023

\********************************************************************/

#ifndef EXCL_TRIGGER01

#define TRIGGER01_COMMON_DEFINED

typedef struct {
  UINT16    event_id;
  UINT16    trigger_mask;
  char      buffer[32];
  INT32     type;
  INT32     source;
  char      format[8];
  BOOL      enabled;
  INT32     read_on;
  INT32     period;
  double    event_limit;
  UINT32    num_subevents;
  INT32     log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
  INT32     write_cache_size;
} TRIGGER01_COMMON;

#define TRIGGER01_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = UINT16 : 1",\
"Trigger mask = UINT16 : 0",\
"Buffer = STRING : [32] BUF01",\
"Type = INT32 : 130",\
"Source = INT32 : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT32 : 1",\
"Period = INT32 : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = UINT32 : 0",\
"Log history = INT32 : 0",\
"Frontend host = STRING : [32] 192.168.2.2",\
"Frontend name = STRING : [32] Frontend with Custom Rate01",\
"Frontend file name = STRING : [256] /home/root/DAQ_soft/sfgddaq/examples/experiment_patch/frontend_customrate.cxx",\
"Status = STRING : [256] Frontend with Custom Rate01@192.168.2.2",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"Write cache size = INT32 : 0",\
"",\
NULL }

#define TRIGGER01_SETTINGS_DEFINED

typedef struct {
  INT32     trigger_interval__ms_;
  INT32     event_size__bytes_;
} TRIGGER01_SETTINGS;

#define TRIGGER01_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Trigger interval (ms) = INT32 : 50",\
"Event size (bytes) = INT32 : 1000",\
"",\
NULL }

#endif

#ifndef EXCL_EB

#define AD01_BANK_DEFINED

typedef struct {
  UINT32    data_size;
  double    mean_word;
} AD01_BANK;

#define AD01_BANK_STR(_name) const char *_name[] = {\
"[.]",\
"Data Size = UINT32 : 1000",\
"Mean word = DOUBLE : 2040.14",\
"",\
NULL }

#define EB_COMMON_DEFINED

typedef struct {
  UINT16    event_id;
  UINT16    trigger_mask;
  char      buffer[32];
  INT32     type;
  INT32     source;
  char      format[8];
  BOOL      enabled;
  INT32     read_on;
  INT32     period;
  double    event_limit;
  UINT32    num_subevents;
  INT32     log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
  INT32     write_cache_size;
} EB_COMMON;

#define EB_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = UINT16 : 1",\
"Trigger mask = UINT16 : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT32 : 0",\
"Source = INT32 : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT32 : 0",\
"Period = INT32 : 0",\
"Event limit = DOUBLE : 0",\
"Num subevents = UINT32 : 0",\
"Log history = INT32 : 0",\
"Frontend host = STRING : [32] t2knd280stream",\
"Frontend name = STRING : [32] Ebuilder",\
"Frontend file name = STRING : [256] /home/ndupgrade/alma9/fgddaq/online/src/sfgdebuser.cxx",\
"Status = STRING : [256] ",\
"Status color = STRING : [32] ",\
"Hidden = BOOL : n",\
"Write cache size = INT32 : 0",\
"",\
NULL }

#define EB_SETTINGS_DEFINED

typedef struct {
  INT32     number_of_fragment;
  BOOL      user_build;
  char      user_field[64];
  BOOL      fragment_required;
  UINT32    fragment_timeout;
  UINT32    end_of_run_timeout;
  BOOL      stop_run_on_error;
} EB_SETTINGS;

#define EB_SETTINGS_STR(_name) const char *_name[] = {\
"[.]",\
"Number of Fragment = INT32 : 1",\
"User build = BOOL : n",\
"User Field = STRING : [64] 100",\
"Fragment Required = BOOL : y",\
"Fragment timeout = UINT32 : 300",\
"End of run timeout = UINT32 : 5",\
"Stop run on error = BOOL : y",\
"",\
NULL }

#endif

#ifndef EXCL_TRIGGER

#define TRIGGER_COMMON_DEFINED

typedef struct {
  UINT16    event_id;
  UINT16    trigger_mask;
  char      buffer[32];
  INT32     type;
  INT32     source;
  char      format[8];
  BOOL      enabled;
  INT32     read_on;
  INT32     period;
  double    event_limit;
  UINT32    num_subevents;
  INT32     log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
  INT32     write_cache_size;
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = UINT16 : 1",\
"Trigger mask = UINT16 : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT32 : 2",\
"Source = INT32 : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT32 : 1",\
"Period = INT32 : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = UINT32 : 0",\
"Log history = INT32 : 0",\
"Frontend host = STRING : [32] 192.168.2.2",\
"Frontend name = STRING : [32] Sample Frontend",\
"Frontend file name = STRING : [256] /home/root/online/TestFrontend/frontend.cxx",\
"Status = STRING : [256] Sample Frontend@192.168.2.2",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"Write cache size = INT32 : 0",\
"",\
NULL }

#endif

#ifndef EXCL_PERIODIC

#define PERIODIC_COMMON_DEFINED

typedef struct {
  UINT16    event_id;
  UINT16    trigger_mask;
  char      buffer[32];
  INT32     type;
  INT32     source;
  char      format[8];
  BOOL      enabled;
  INT32     read_on;
  INT32     period;
  double    event_limit;
  UINT32    num_subevents;
  INT32     log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
  INT32     write_cache_size;
} PERIODIC_COMMON;

#define PERIODIC_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = UINT16 : 2",\
"Trigger mask = UINT16 : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT32 : 1",\
"Source = INT32 : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT32 : 377",\
"Period = INT32 : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = UINT32 : 0",\
"Log history = INT32 : 10",\
"Frontend host = STRING : [32] 192.168.2.2",\
"Frontend name = STRING : [32] Sample Frontend",\
"Frontend file name = STRING : [256] /home/root/online/TestFrontend/frontend.cxx",\
"Status = STRING : [256] Sample Frontend@192.168.2.2",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"Write cache size = INT32 : 0",\
"",\
NULL }

#endif

#ifndef EXCL_OCB

#define OCB_COMMON_DEFINED

typedef struct {
  UINT16    event_id;
  UINT16    trigger_mask;
  char      buffer[32];
  INT32     type;
  INT32     source;
  char      format[8];
  BOOL      enabled;
  INT32     read_on;
  INT32     period;
  double    event_limit;
  UINT32    num_subevents;
  INT32     log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
  INT32     write_cache_size;
} OCB_COMMON;

#define OCB_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = UINT16 : 1",\
"Trigger mask = UINT16 : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT32 : 2",\
"Source = INT32 : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT32 : 1",\
"Period = INT32 : 100",\
"Event limit = DOUBLE : 0",\
"Num subevents = UINT32 : 0",\
"Log history = INT32 : 0",\
"Frontend host = STRING : [32] t2knd280server-data",\
"Frontend name = STRING : [32] OCB Frontend",\
"Frontend file name = STRING : [256] /home/ocb/SFGD_DAQ/sfgddaq/ocb-interface-VST/online/OCBRead/frontend.cxx",\
"Status = STRING : [256] OCB Frontend@t2knd280server-data",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"Write cache size = INT32 : 0",\
"",\
NULL }

#endif

#ifndef EXCL_OCBPERIODIC

#define OCBPERIODIC_COMMON_DEFINED

typedef struct {
  UINT16    event_id;
  UINT16    trigger_mask;
  char      buffer[32];
  INT32     type;
  INT32     source;
  char      format[8];
  BOOL      enabled;
  INT32     read_on;
  INT32     period;
  double    event_limit;
  UINT32    num_subevents;
  INT32     log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
  BOOL      hidden;
  INT32     write_cache_size;
} OCBPERIODIC_COMMON;

#define OCBPERIODIC_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = UINT16 : 2",\
"Trigger mask = UINT16 : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT32 : 1",\
"Source = INT32 : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT32 : 377",\
"Period = INT32 : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = UINT32 : 0",\
"Log history = INT32 : 10",\
"Frontend host = STRING : [32] t2knd280server-data",\
"Frontend name = STRING : [32] OCB Frontend",\
"Frontend file name = STRING : [256] /home/ocb/SFGD_DAQ/sfgddaq/ocb-interface-VST/online/OCBRead/frontend.cxx",\
"Status = STRING : [256] OCB Frontend@t2knd280server-data",\
"Status color = STRING : [32] greenLight",\
"Hidden = BOOL : n",\
"Write cache size = INT32 : 0",\
"",\
NULL }

#endif

