/********************************************************************\

  Name:         frontend.c
  Created by:   Stefan Ritt

  Contents:     Experiment specific readout code (user part) of
                Midas frontend. This example simulates a "trigger
                event" and a "periodic event" which are filled with
                random data.
 
                The trigger event is filled with two banks (ADC0 and TDC0),
                both with values with a gaussian distribution between
                0 and 4096. About 100 event are produced per second.
 
                The periodic event contains one bank (PRDC) with four
                sine-wave values with a period of one minute. The
                periodic event is produced once per second and can
                be viewed in the history system.

\********************************************************************/

#undef NDEBUG // midas required assert() to be always enabled

// #define WRITE_TO_FILE

#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h> // assert()

#include "midas.h"
#include "experim.h"

#include "mfe.h"
// include the OCB header
#include "OCB_interface.h"

#include <chrono>
typedef std::chrono::high_resolution_clock Clock;
using std::chrono::nanoseconds;
using std::chrono::microseconds;
using std::chrono::duration_cast;
using namespace std;

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
const char *frontend_name = "OCB Frontend";
/* The frontend file name, don't change it */
const char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 3000;

/* maximum event size produced by this frontend */
INT max_event_size = 1024 * 1024; // 1 MB

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024; // 5 MB

/* buffer size to hold events */
INT event_buffer_size = 10 * 1024 * 1024; // 10 MB, must be > 2 * max_event_size

int Flag_EndOfRun=0;

// write data to output binary file
float currsize = 0;

// temperatory save the data to confirm the data quality
//FILE *fp;
const char *filename="test.bin";

/*-- Function declarations -----------------------------------------*/

INT frontend_init(void);
INT frontend_exit(void);
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop(void);

INT read_trigger_event(char *pevent, INT off);
INT read_periodic_event(char *pevent, INT off);

INT poll_event(INT source, INT count, BOOL test);
INT interrupt_configure(INT cmd, INT source, POINTER_T adr);

/*-- Equipment list ------------------------------------------------*/
int HistBinning(int inputNo, int fMode);

BOOL equipment_common_overwrite = TRUE;

EQUIPMENT equipment[] = {

   {"OCB",               /* equipment name */
      {1, 0,                 /* event ID, trigger mask */
         "BUFOCB",           /* event buffer */
         EQ_POLLED | EQ_EB,          /* equipment type */
         0,                  /* event source */
         "MIDAS",            /* format */
         TRUE,               /* enabled */
         RO_RUNNING,        /* read only when running */
         100,                /* poll for 100ms */
         0,                  /* stop run after this event limit */
         0,                  /* number of sub events */
         0,                  /* don't log history */
         "", "", "", "", "", 0, 0},
      read_trigger_event,    /* readout routine */
   },

   {""}
};

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
   /* put any hardware initialization here */
   //Test OCB code here
   OCB::open();

   /* print message and return FE_ERR_HW if frontend should not be started */
   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{

   //Test OCB code here
   OCB::close();

   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
   std::cout << "start begin_of_run" << std::endl;
   /* put here clear scalers etc. */

   //Test OCB code here
   OCB::set_option_status OCBstatus;
   OCBstatus = OCB::readout_enable();
   if(OCBstatus == OCB::set_option_status::error) {
     sprintf(error, "Failed enabling readout on OCB");
     return 0;
   }

   OCBstatus = OCB::trig_enable();
   if(OCBstatus == OCB::set_option_status::error) {
     sprintf(error, "Failed enabling trigger on OCB");
     return 0;
   }

#ifdef WRITE_TO_FILE
	 /*
   fp = fopen(filename, "wb");
   if (fp == NULL) {
         printf("Error opening file %s\n", filename);
         return 0;
   }
	 */
#endif
   Flag_EndOfRun=0;

	 std::cout << "end begin_of_run" << std::endl;

   return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
	 std::cout << "start end_of_run" << endl;
   //Test OCB code here
   OCB::set_option_status OCBstatus = OCB::readout_disable();
   if(OCBstatus == OCB::set_option_status::error) {
     sprintf(error, "Failed disabling readout on OCB");
     return 0;
   }

   // write remaining data in FIFO (if any) to output binary file
   int loopCount=0;
   printf("Write remaining data (if any) to output file ...\n");
   while (OCB::get_fifo_status()==OCB::trigger_status::trigger_available){
         int data = OCB::read_event_simple();
#ifdef WRITE_TO_FILE
        // fwrite(&data, sizeof(data), 1, fp);
#endif
 				 loopCount++;
   	     printf("There are still %d remaining lines ... \n", loopCount);
   }
   printf("FIFO is empty now! \n");
   Flag_EndOfRun=1;

	 std::cout << "end end_of_run" << endl;

   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
	 std::cout << "start pause_of_run" << endl;
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
	 std::cout << "start resuem_of_run" << endl;
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
	 std::cout << "start frontend_loop" << endl;
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
   return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;
   DWORD flag;

   if(Flag_EndOfRun) return 0;

   for (i = 0; i < count; i++) {
      /* poll hardware and set flag to TRUE if new event is available */
      if (!test){
         flag = (OCB::get_trigger_status()==OCB::trigger_status::trigger_available);
         if (flag){
             return TRUE;
         }
				 else {
					   printf("Warning: stuck in this poll event. Check the MCM to see whether it is still runing! \n");
             ss_sleep(1); // sleep for 1 milisecond.
						 return 0;
				 }
	 		}
   }

   return 0;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{
   UINT32 *pdata;

   /* init bank structure */
   bk_init32(pevent);

   /* create structured ADC0 bank */
   bk_create(pevent, "ADC0", TID_UINT32, (void **)&pdata);

   /* following code to "simulates" some ADC data */
/*
   float filesize=0.001*1e6;
   while (OCB::get_trigger_status()==OCB::trigger_status::trigger_available) {
   		fflush(stdout);
   
//      // test 1
//   		uint32_t data = OCB::read_event_simple();
//   		fwrite(&data, sizeof(data), 1, fp);
//      *pdata++ = data;

		  // test 2
      size_t nbyte = OCB::read_event_line(pdata, fp);
      pdata+=nbyte;

   		currsize += nbyte;
      if (currsize>=filesize) break;
   }
   printf("Progress: %.3f KB of %.3f MB written ...\n", currsize/1e3, filesize/1e6);
*/
   // test 3
   fflush(stdout);
   //DWORD start_time=ss_millitime();
   auto clock_t1=Clock::now();
#ifdef WRITE_TO_FILE
   //size_t nbyte = OCB::read_event(pdata, max_event_size/2., fp);
#else
   size_t nbyte = OCB::read_event(pdata, max_event_size/2., NULL);
#endif
   pdata+=nbyte;
   currsize += nbyte;
   auto clock_t2=Clock::now();
   //DWORD end_time=ss_millitime();
   int clock_duration1=duration_cast<nanoseconds>(clock_t2-clock_t1).count()/1E3;
#ifdef WRITE_TO_FILE
	 /*
   FILE* fp2;
   char filename2[1024];
   sprintf(filename2, "tempfile_OCB_run%06d.log", run_number);
   fp2=fopen(filename2,"a");
   if (fp2 == NULL) {
         printf("Error opening file %s\n", filename2);
			   abort();
   }
   //fprintf(fp2, "% d\n", end_time-start_time);
   fprintf(fp2, "% d\n", clock_duration1);
   fclose(fp2);

   printf("Progress: %.3f KB written ...\n", currsize/1e3);
	 */
#endif

   bk_close(pevent, pdata);

   /* limit event rate to 100 Hz. In a real experiment remove this line */
   //ss_sleep(10);

   return bk_size(pevent);
}

/*-- Periodic event ------------------------------------------------*/

INT read_periodic_event(char *pevent, INT off)
{
   UINT32 *pdata;

   /* init bank structure */
   bk_init32(pevent);

   /* create SCLR bank */
   bk_create(pevent, "PRDC", TID_UINT32, (void **)&pdata);

   /* following code "simulates" some values in sine wave form */
   for (int i = 0; i < 16; i++)
      *pdata++ = 100*sin(M_PI*time(NULL)/60+i/2.0)+100;

   bk_close(pevent, pdata);

   return bk_size(pevent);
}

