#include "wordutils.h"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " inname" << endl;
        return 1;
    }
    
    string filename(argv[1]);
    ifstream fin(filename.c_str(), ios::binary);
    if (!fin) {
        cerr << "Could not open " << filename << endl;
        return 2;
    }

    uint32_t word;
    while (!fin.eof()) {
      fin.read((char *)&word, sizeof(word));
      printWord(word);
    }

    return 0;
}
