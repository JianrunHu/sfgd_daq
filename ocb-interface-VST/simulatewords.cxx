#include "wordutils.h"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " outname" << endl;
        return 1;
    }
    
    string filename(argv[1]);
    ofstream fout(filename.c_str(), ios::binary);
    if (!fout) {
        cerr << "Could not open " << filename << endl;
        return 2;
    }
    
    size_t buffer_size = 1000000*sizeof(uint32_t);
    uint32_t *buffer = new uint32_t[buffer_size/sizeof(uint32_t)];
    
    uint32_t eventNumber = 0;
    uint32_t GTSTag = 0;
    uint32_t gateNumber = 0;
    size_t nbytes = simulateOCBEvent(buffer_size, buffer, eventNumber, GTSTag, gateNumber);
    
    fout.write(reinterpret_cast<char*>(buffer), nbytes);
    delete [] buffer;
    
    fout.close();

    return 0;
}

