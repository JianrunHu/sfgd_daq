#include "OCB_interface.h"

#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>

using namespace std;

int main(int argc, char *argv[]) {
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " outname" << endl;
        return 1;
    }

    string filename(argv[1]);
    ofstream fout(filename.c_str(), ios::binary);
    if (!fout) {
        cerr << "Could not open " << filename << endl;
        return 2;
    }

    size_t buffer_size = 1000000;
    char *buffer = new char[buffer_size];

    OCB::open();

    OCB::readout_enable();
    OCB::trig_enable();

    int nevents = 2;
    int ievent = 0;
    while (ievent < nevents) {
        if (OCB::get_trigger_status() == OCB::trigger_status::no_trigger) {
            usleep(10);
            continue;
        }
        size_t nbytes = OCB::read_event(buffer, buffer_size, NULL);
        cout << "Write " << nbytes << " bytes into output file" << endl;
        fout.write(buffer, nbytes);
        OCB::trig_enable();
        ievent++;
    }

    OCB::readout_disable();
    OCB::close();

    delete [] buffer;
    fout.close();

    return 0;
}


