#include "wordutils.h"
#include "BufferManager.h"

#include <cstdio>

uint32_t isolateNbits(uint32_t word, int startBit, int Nbits){
  unsigned r = ((1 << Nbits) - 1); 
  return (word >> startBit) & r;
}

uint32_t setNbits(uint32_t word, int startBit, int Nbits, uint32_t val){
  unsigned mask = ((1 << Nbits) - 1) << startBit; // e.g. 00001100 for start=2,N=2
  return (word & ~mask) | ((val << startBit) & mask);
}

// gateHeader=0
void printGateHeader(uint32_t word) {
  uint32_t boardID = isolateNbits(word, 20, 8);
  uint32_t headerType = isolateNbits(word, 19, 1);
  printf("Gate header: board ID = %3d, header ", boardID);
  if (headerType == 0) {
    uint32_t gateType = isolateNbits(word, 16, 3);
    uint32_t gateNumber = isolateNbits(word, 0, 16);
    printf("A, gate type = %d, gate number = %5d\n", gateType, gateNumber);
  }
  else if (headerType == 1) {
    uint32_t gateTimeFromGTS = isolateNbits(word, 0, 11);
    printf("B, gate time from GTS = %5d\n", gateTimeFromGTS);
  }
  else {
    printf("(unknown)\n");
  }
}

uint32_t makeGateHeaderA(uint32_t boardID, uint32_t gateType, uint32_t gateNumber) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::gateHeader));
  word = setNbits(word, 20,  8, boardID);
  word = setNbits(word, 19,  1, 0);
  word = setNbits(word, 16,  3, gateType);
  word = setNbits(word,  0, 16, gateNumber);
  return word;
}

uint32_t makeGateHeaderB(uint32_t boardID, uint32_t gateTimeFromGTS) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::gateHeader));
  word = setNbits(word, 20,  8, boardID);
  word = setNbits(word, 19,  1, 1);
  word = setNbits(word,  0, 11, gateTimeFromGTS);
  return word;
}

// gateTrailer=6
void printGateTrailer(uint32_t word) {
  uint32_t boardID = isolateNbits(word, 20, 8);
  uint32_t gateType = isolateNbits(word, 16, 3);
  uint32_t gateNumber = isolateNbits(word, 0, 16);
  printf("Gate trailer: board ID = %3d, gate type = %d, gate number = %5d\n", boardID, gateType, gateNumber);
}

uint32_t makeGateTrailer(uint32_t boardID, uint32_t gateType, uint32_t gateNumber) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::gateHeader));
  word = setNbits(word, 20,  8, boardID);
  word = setNbits(word, 19,  1, 0);
  word = setNbits(word, 16,  3, gateType);
  word = setNbits(word,  0, 16, gateNumber);
  return word;
}


// GTSHeader=1
// Eric's code also has the tagid using 0~1
void printGTSHeader(uint32_t word) {
  uint32_t GTSTag = isolateNbits(word, 0, 28);
  printf("GTS header: GTS tag = %8d\n", GTSTag);
}

uint32_t makeGTSHeader(uint32_t GTSTag) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::GTSHeader));
  word = setNbits(word,  0, 28, GTSTag);
  return word;
}

// hitTime=2
void printHitTime(uint32_t word) {
  uint32_t channelID = isolateNbits(word, 20, 8);
  uint32_t hitID = isolateNbits(word, 17, 3);
  uint32_t tagID = isolateNbits(word, 15, 2);
  uint32_t edge = isolateNbits(word, 14, 1);
  uint32_t hitTime = isolateNbits(word, 0, 14);
  printf("Hit time: channel ID = %3d, hit ID = %d, tag ID = %d, edge = %d, hit time = %4d\n", channelID, hitID, tagID, edge, hitTime);
}

uint32_t makeHitTime(uint32_t channelID, uint32_t hitID, uint32_t tagID, uint32_t edge, uint32_t hitTime) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::hitTime));
  word = setNbits(word, 20,  8, channelID);
  word = setNbits(word, 17,  3, hitID);
  word = setNbits(word, 15,  2, tagID);
  word = setNbits(word, 14,  1, edge);
  word = setNbits(word,  0, 14, hitTime);
  return word;
}

void getHitTime(uint32_t word, uint32_t &channelID, uint32_t &hitID, uint32_t &tagID, uint32_t &edge, uint32_t &hitTime){
  channelID = isolateNbits(word, 20, 8);
  hitID = isolateNbits(word, 17, 3);
  tagID = isolateNbits(word, 15, 2);
  edge = isolateNbits(word, 14, 1);
  hitTime = isolateNbits(word, 0, 14);
}

// hitAmplitude=3
void printHitAmplitude(uint32_t word) {
  uint32_t channelID = isolateNbits(word, 20, 8);
  uint32_t hitID = isolateNbits(word, 17, 3);
  uint32_t tagID = isolateNbits(word, 15, 2);
  uint32_t amplitudeID = isolateNbits(word, 12, 3);
  uint32_t amplitude = isolateNbits(word, 0, 12);
  printf("Hit ampl: channel ID = %3d, hit ID = %d, tag ID = %d, amplitude ID = %d, amplitude = %4d\n", channelID, hitID, tagID, amplitudeID, amplitude);
}

uint32_t makeHitAmplitude(uint32_t channelID, uint32_t hitID, uint32_t tagID, uint32_t amplitudeID, uint32_t amplitude) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::hitAmplitude));
  word = setNbits(word, 20,  8, channelID);
  word = setNbits(word, 17,  3, hitID);
  word = setNbits(word, 15,  2, tagID);
  word = setNbits(word, 12,  3, amplitudeID);
  word = setNbits(word,  0, 12, amplitude);
  return word;
}

void getHitAmplitude(uint32_t word, uint32_t &channelID, uint32_t &hitID, uint32_t &tagID, uint32_t &amplitudeID, uint32_t &amplitude){
  channelID = isolateNbits(word, 20, 8);
  hitID = isolateNbits(word, 17, 3);
  tagID = isolateNbits(word, 15, 2);
  amplitudeID = isolateNbits(word, 12, 3);
  amplitude = isolateNbits(word, 0, 12);
}

// GTSTrailer1 = 4
void printGTSTrailer1(uint32_t word) {
  uint32_t GTSTag = isolateNbits(word, 0, 28);
  printf("GTS Trailer: GTS tag = %8d\n", GTSTag);
}

uint32_t makeGTSTrailer1(uint32_t GTSTag) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::GTSTrailer1));
  word = setNbits(word,  0, 28, GTSTag);
  return word;
}

// GTSTrailer2 = 5
void printGTSTrailer2(uint32_t word) {
  uint32_t GTSTime = isolateNbits(word, 0, 20);
  printf("GTS Trailer: GTS time = %8d\n", GTSTime);
}

uint32_t makeGTSTrailer2(uint32_t GTSTime) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::GTSTrailer2));
  word = setNbits(word,  0, 20, GTSTime);
  return word;
}

// gateTime = 7
void printGateTime(uint32_t word) {
  uint32_t GateTime = isolateNbits(word, 0, 28);
  printf("Gate Time: Gate time = %8d\n", GateTime);
}

uint32_t makeGateTime(uint32_t GateTime) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::gateTime));
  word = setNbits(word,  0, 28, GateTime);
  return word;
}

// holdTime = 11
void printHoldTime(uint32_t word) {
  uint32_t boardID = isolateNbits(word, 20, 8);
  uint32_t headerType = isolateNbits(word, 19, 1);
  uint32_t holdTime = isolateNbits(word, 0, 11);
  printf("Hold header: board ID = %3d, header ", boardID);
  if(headerType==0){
  printf("A, hold start time from GTRIG TS = %5d\n", holdTime);
  }
  else if(headerType==0){
  printf("B, hold stop time from GTRIG TS = %5d\n", holdTime);
  }
  else {
    printf("(unknown)\n");
  }
}

uint32_t makeHoldTime(uint32_t boardID, uint32_t headerType, uint32_t holdTime) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::gateHeader));
  word = setNbits(word, 20,  8, boardID);
  word = setNbits(word, 19,  1, headerType);
  word = setNbits(word,  0, 11, holdTime);
  return word;
}

// OCBEvtStart = 8
void printOCBEvtStart(uint32_t word) {
  uint32_t gateType = isolateNbits(word,25,3); 
  uint32_t gateTag  = isolateNbits(word,23,2); 
  uint32_t eventNumber = isolateNbits(word, 0, 23); 
  printf("\n");
  printf("OCB SYNC Word: gate type = %d, gate tag = %d, event number = %d\n", gateType, gateTag, eventNumber);
}

uint32_t makeOCBEvtStart(uint32_t gateType, uint32_t gateTag, uint32_t eventNumber) {
  uint32_t word = 0;
  word = setNbits(word, 28,  4, int(typeID::OCBEvtStart));
  word = setNbits(word, 25,  3, gateType); 
  word = setNbits(word, 23,  2, gateTag); 
  word = setNbits(word,  0, 23, eventNumber); 
  return word;
}

// OCBEvtEnd = 9
void printOCBEvtEnd(uint32_t word) {
  printf("OCB Trailer Word: \n", word);
	if (isolateNbits(word, 0, 16) == 0) printf("Data:0x%08X\tOCB Trailer Word No error\r\n", word);
	else {
		if (isolateNbits(word, 15, 1) == 1) printf("Data:0x%08X\tOCB Trailer Word\tGate open timeout\r\n", word);
		if (isolateNbits(word, 14, 1) == 1) printf("Data:0x%08X\tOCB Trailer Word\tGate close error\r\n", word);
    int Nfeb=14;
	  for (int i=0; i<Nfeb; ++i) {
			if (isolateNbits(word, 13-i, 1) == 1) printf("Data:0x%08X\tOCB Trailer Word\tDAQ unit %d error\r\n", word, 13-i);
	 	}
	}
	printf("\r\n");
}

uint32_t makeOCBEvtEnd() { // No error for MC
  uint32_t word = 0;
  word = setNbits(word,  0, 16, 0); 
  return word;
}

// print word
void printWord(uint32_t word) {
  switch (isolateNbits(word, 28, 4)) {
    case int(typeID::gateHeader): //0
      printGateHeader(word);
      break;
      
    case int(typeID::GTSHeader): //1
      printGTSHeader(word);
      break;
      
    case int(typeID::hitTime): //2
      printHitTime(word);
      break;
      
    case int(typeID::hitAmplitude): //3
      printHitAmplitude(word);
      break;
      
    case int(typeID::GTSTrailer1): //4
      printGTSTrailer1(word);
      break;
      
    case int(typeID::GTSTrailer2): //5
      printGTSTrailer2(word);
      break;
      
    case int(typeID::gateTrailer): //6
      printGateTrailer(word);
      break;
      
    case int(typeID::gateTime): //7
      printGateTime(word);
      break;
      
    case int(typeID::OCBEvtStart): //8
      printOCBEvtStart(word);
      break;
      
    case int(typeID::OCBEvtEnd): //9
      printOCBEvtEnd(word);
      break;
      
    case int(typeID::holdTime): //11
      printHoldTime(word);
      break;
      
    default:
      printf("Unmatched word: %08x\n", word);
  }
}


std::size_t simulateFEBEvent(std::size_t buffer_size, uint32_t *buffer, uint32_t boardID, uint32_t &GTSTag, uint32_t gateNumber) {
  
  BufferManager bm(buffer_size, buffer);
  
  bm.write(makeGTSHeader(GTSTag));
  GTSTag++;
  
  uint32_t gateTimeFromGTS = rand();
  bm.write(makeGateHeaderA(boardID, 2, gateNumber));
  bm.write(makeGateHeaderB(boardID, gateTimeFromGTS));
  
  // Unmatched word: 7009cab7
  
  for (int ihits = 0; ihits < 3; ihits++) {
    bm.write(0x409749ad);
    bm.write(0x50000000);

    bm.write(makeGTSHeader(GTSTag));
    GTSTag++;

    int channelID = 0;
    int hitID = rand() % 10;
    int tagID = rand() % 10;
    int hitTime = rand() % 4096;

    bm.write(makeHitTime(channelID, hitID, tagID, 0, hitTime));
    hitTime += rand() % 16;
    bm.write(makeHitTime(channelID, hitID, tagID, 1, hitTime));
    hitTime += rand() % 300;
    hitID++;

    bm.write(makeHitTime(channelID, hitID, tagID, 0, hitTime));
    hitTime += rand() % 16;
    bm.write(makeHitTime(channelID, hitID, tagID, 1, hitTime));
    hitTime += rand() % 300;
    hitID++;

    uint32_t amplitude = rand() % 10000;
    bm.write(makeHitAmplitude(channelID, hitID, tagID, 3, amplitude/200));
    bm.write(makeHitAmplitude(channelID, hitID, tagID, 2, amplitude));
  }
  
  bm.write(0x6ff0000f);
  bm.write(0x7009cab7);
  
  bm.write(0x409749b0);
  bm.write(0x58000003);
  
  bm.write(makeGTSHeader(GTSTag));
  GTSTag++;
  
  bm.write(0x409749b1);
  bm.write(0x50000004);
  
  bm.write(makeGTSHeader(GTSTag));
  GTSTag++;

  bm.write(0x409749b2);
  bm.write(0x50000005);
  
  // end of event
  
  return buffer_size - bm.getSize();
}

std::size_t simulateOCBEvent(std::size_t buffer_size, uint32_t *buffer, uint32_t eventNumber, uint32_t &GTSTag, uint32_t gateNumber) {
  uint32_t gateType = 1;
  
  BufferManager bm(buffer_size, buffer);
  bm.write(makeOCBEvtStart(gateType, 2, eventNumber)); 
  
  // in principle I think here we could have a loop over multiple FEBs
  // {
  //   int boardID = 255;
  //   std::size_t nbytes = simulateFEBEvent(buffer, boardID, GTSTag, gateNumber);
  //   buffer += nbytes / sizeof(*buffer);
  // }
  
  for (int boardID = 0; boardID < 255; boardID++) {
    size_t nbytes = simulateFEBEvent(bm.getSize(), bm.getBuffer(), boardID, GTSTag, gateNumber);
    bm.didWriteBytes(nbytes);
  }

  bm.write(makeOCBEvtEnd()); 
  
  return buffer_size - bm.getSize();
}
