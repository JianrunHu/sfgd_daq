#pragma once

#include <cinttypes>
#include <cstddef>

enum class typeID {
  gateHeader = 0,
  GTSHeader = 1,
  hitTime = 2,
  hitAmplitude = 3,
  GTSTrailer1 = 4,
  GTSTrailer2 = 5,
  gateTrailer = 6,
  gateTime = 7,
  OCBEvtStart = 8, // OCB protocol
  OCBEvtEnd = 9, // OCB protocol
  holdTime = 11,
  OCB_FEBTimeOut=13,  // No implemented yet
  OCB_SpecialWord=15, // No implemented yet
};

uint32_t isolateNbits(uint32_t word, int startBit, int Nbits);
uint32_t setNbits(uint32_t word, int startBit, int Nbits, uint32_t val);

// gateHeader=0
void printGateHeader(uint32_t word);
uint32_t makeGateHeaderA(uint32_t boardID, uint32_t gateType, uint32_t gateNumber);
uint32_t makeGateHeaderB(uint32_t boardID, uint32_t gateTimeFromGTS);

// gateTrailer=6
void printGateTrailer(uint32_t word);
uint32_t makeGateTrailer(uint32_t boardID, uint32_t gateType, uint32_t gateNumber);

// GTSHeader=1
void printGTSHeader(uint32_t word);
uint32_t makeGTSHeader(uint32_t GTSTag);

// hitTime=2
void printHitTime(uint32_t word);
uint32_t makeHitTime(uint32_t channelID, uint32_t hitID, uint32_t tagID, uint32_t edge, uint32_t hitTime);
void getHitTime(uint32_t word, uint32_t &channelID, uint32_t &hitID, uint32_t &tagID, uint32_t &edge, uint32_t &hitTime);

// hitAmplitude=3
void printHitAmplitude(uint32_t word);
uint32_t makeHitAmplitude(uint32_t channelID, uint32_t hitID, uint32_t tagID, uint32_t amplitudeID, uint32_t amplitude);
void getHitAmplitude(uint32_t word, uint32_t &channelID, uint32_t &hitID, uint32_t &tagID, uint32_t &amplitudeID, uint32_t &amplitude);

// GTSTrailer1 = 4
void printGTSTrailer1(uint32_t word);
uint32_t makeGTSTrailer1(uint32_t GTSTag);

// GTSTrailer2 = 5
void printGTSTrailer2(uint32_t word);
uint32_t makeGTSTrailer2(uint32_t GTSTime);

// gateTime = 7
void printGateTime(uint32_t word);
uint32_t makeGateTime(uint32_t GateTime);

// holdTime = 11
void printHoldTime(uint32_t word);
uint32_t makeHoldTime(uint32_t boardID, uint32_t holdTime);

// OCBEvtStart = 8
void printOCBEvtStart(uint32_t word);
uint32_t makeOCBEvtStart(uint32_t gateType, uint32_t gateTag, uint32_t eventNumber);

// OCBEvtEnd = 9
void printOCBEvtEnd(uint32_t word);
uint32_t makeOCBEvtEnd(); // No Error for MC

void printWord(uint32_t word);


std::size_t simulateFEBEvent(std::size_t buffer_size, uint32_t *buffer, uint32_t boardID, uint32_t &GTSTag, uint32_t gateNumber);

std::size_t simulateOCBEvent(std::size_t buffer_size, uint32_t *buffer, uint32_t eventNumber, uint32_t &GTSTag, uint32_t gateNumber);

